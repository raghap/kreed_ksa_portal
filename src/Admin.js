import React, { Component } from 'react';
import { render } from 'react-dom'

import Master from './containers/Master'
import App from './containers/App'
import Fireadmin from './containers/Fireadmin'
import Firestoreadmin from './containers/Firestoreadmin'
import CoachList from './containers/CoachList';
import AthleteList from './containers/AthleteList';
import MeetList from './containers/MeetList';
import VenueList from './containers/VenueList';
import MeetRegisteredList from './containers/MeetRegisteredList';
import ChangePassword from './containers/ChangePassword';
import Push from './containers/Push'
import Config from   './config/app';

import { Router, Route,hashHistory,IndexRoute } from 'react-router'

class Admin extends Component {

  //Prints the dynamic routes that we need for menu of type fireadmin
  getFireAdminRoutes(item){
    if(item.link=="fireadmin"){
      return (<Route path={"/fireadmin/"+item.path} component={Fireadmin}/>)
    }else{

    }
  }

  //Prints the dynamic routes that we need for menu of type fireadmin
  getFireAdminSubRoutes(item){
    if(item.link=="fireadmin"){
      return (<Route path={"/fireadmin/"+item.path+"/:sub"} component={Fireadmin}/>)
    }else{

    }
  }

  //Prints the Routes
  /*
  {Config.adminConfig.menu.map(this.getFireAdminRoutes)}
  {Config.adminConfig.menu.map(this.getFireAdminSubRoutes)}
  */
  render() {
    return (
      <Router history={hashHistory}>
          <Route path="/" component={Master}>
            {/* make them children of `Master` */}
            <IndexRoute component={App}></IndexRoute>
            <Route path="/app" component={App}/>
            <Route path="/push" component={Push}/>

            <Route path="/assoc" component={Firestoreadmin}/>
            <Route path="/assoc/:sub" component={Firestoreadmin}/>

            <Route path="/affiliate" component={CoachList}/>
            <Route path="/affiliate/:sub" component={CoachList}/>

            <Route path="/athlete" component={AthleteList}/>
            <Route path="/athlete/:sub" component={AthleteList}/>

            <Route path="/venues" component={VenueList}/>
            <Route path="/venues/:sub" component={VenueList}/>

            <Route path="/meets" component={MeetList}/>
            <Route path="/meets/:sub" component={MeetList}/> 

            <Route path="/registered" component={MeetRegisteredList}/>
            <Route path="/registered/:sub" component={MeetRegisteredList}/>

            <Route path="/ChangePassword" component={ChangePassword}/>
            <Route path="/ChangePassword/:sub" component={ChangePassword}/>

          </Route>
        </Router>
    );
  }

}

export default Admin;
