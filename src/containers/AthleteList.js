import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom';
import Indicator from '../components/Indicator'
import { Link } from 'react-router'
import firebase from '../config/database'
import Fields from '../components/fields/Fields.js'
import Input from '../components/fields/Input.js';
import { TextArea } from '../components/fields';
import Table from '../components/tables/Table.js'
import MyTable from '../components/tables/MyTable.js'
import DocsTable from '../components/tables/DocsTable.js'
import Config from '../config/app';
import Common from '../common.js';
import Notification from '../components/Notification';
import SkyLight from 'react-skylight';
import INSERT_STRUCTURE from "../config/firestoreschema.js"
import FirebasePaginator from "firebase-paginator"
import NavBar from '../components/NavBar'
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';
import SelectDropdown from '../components/fields/SelectDropdown.js';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import FileUploader from 'react-firebase-file-uploader';

import * as firebaseREF from 'firebase';
require("firebase/firestore");
require("firebase/functions");

import { Accordion, AccordionItem } from 'react-light-accordion';
import 'react-light-accordion/demo/css/index.css';
import { truncateSync } from 'fs';

const ROUTER_PATH = "/athlete/";
const placeholder = "clubID";

var Loader = require('halogen/PulseLoader');

class AthleteList extends Component {

  constructor(props) {
    super(props);

    //Create initial step
    this.state = {
      athletesByClub: [],
      documents: [],
      collections: [],
      currentCollectionName: "",
      isCollection: false,
      isDocument: false,
      keyToDelete: null,
      pathToDelete: null,
      theSubLink: null,
      fieldsOfOnsert: null,
      isLoading: true,
      showAddCollection: "",
      moveAthleteAlert: false,
      activateAthleteAlert: false,
      deactivateAthleteAlert: false,
      clubsList: [],
      fromClub:"",
      to_club:"",
      links:[],
      note:"",
      Kr_ID: "",
      selectedOption: "",
      selectedCoach:"",
      to_coach:"",
      clubCoaches:[],
      changeAthleteErrorAlert: false,
      documentsLoadingAlert: false,
      errorStatement: "",
      movingAlert: false,
      isLoadingCoaches:false,
      activateKreedId:"",

      avatarURL:"",
      isUploading:false,
      dlProgress: new Map(),
    };

    //Bind function to this
    this.getCollectionDataFromFireStore = this.getCollectionDataFromFireStore.bind(this);
    this.resetDataFunction = this.resetDataFunction.bind(this);
    this.processRecords = this.processRecords.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
    this.cancelAddFirstItem = this.cancelAddFirstItem.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.deleteFieldAction = this.deleteFieldAction.bind(this);
    this.refreshDataAndHideNotification = this.refreshDataAndHideNotification.bind(this);
    this.addKey = this.addKey.bind(this);
    this.showSubItems = this.showSubItems.bind(this);
    this.updatePartOfObject = this.updatePartOfObject.bind(this);
    this.addDocumentToCollection = this.addDocumentToCollection.bind(this);
    this.addItemToArray = this.addItemToArray.bind(this);
    this.export2csv = this.export2csv.bind(this);
    this.moveAthleteAlert = this.moveAthleteAlert.bind(this);
    this.saveMoveAthleteAlert = this.saveMoveAthleteAlert.bind(this);
    this.closeMoveAthleteAlert = this.closeMoveAthleteAlert.bind(this);
    // activate athlete
    this.activateAthleteAlert = this.activateAthleteAlert.bind(this);
    this.saveActivateAthleteAlert = this.saveActivateAthleteAlert.bind(this);
    this.cancelActivateAthleteAlert = this.cancelActivateAthleteAlert.bind(this);
    this.activateAthlete = this.activateAthlete.bind(this);
    // deactivate athlete
    this.deactivateAthleteAlert = this.deactivateAthleteAlert.bind(this);
    this.saveDeactivateAthleteAlert = this.saveDeactivateAthleteAlert.bind(this);
    this.cancelDeactivateAthleteAlert = this.cancelDeactivateAthleteAlert.bind(this);
    this.deactivateAthlete = this.deactivateAthlete.bind(this);
     //Alerts related
     this.hideAlert = this.hideAlert.bind(this);
     this.showAlert = this.showAlert.bind(this);

    //  file uploader
    this.displayList = this.displayList.bind(this);
    this.handleUploadStart = this.handleUploadStart.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleUploadError = this.handleUploadError.bind(this);
    this.handleUploadSuccess = this.handleUploadSuccess.bind(this);
  }
  handleChange = (selectedOption) => {
    var club_coaches = [];
    var db = firebase.app.firestore();
    this.setState({isLoadingCoaches: true});
    var path = "aquaticsID/India/associations/KSA/clubs/" + selectedOption.value  + "/coaches";
    db.collection(path).get()
      .then(querySnapshot => {
        querySnapshot.forEach(function (doc) {
          var vals = doc.data();
          club_coaches.push({value : doc.id , label : vals.name});
        })
        this.setState({ clubCoaches: club_coaches, isLoadingCoaches:false  });
      })
      .catch(error => {
        console.log("Error getting coach list: ", error);
      })
    this.setState({ selectedOption , to_club:selectedOption.value });
  }
  
  handleCoachChange = (selectedCoach) => {
    this.setState({ selectedCoach, to_coach: selectedCoach.label });
    // console.log(`Selected: ${selectedOption.label}`);
  }

    //sweet alerts for delete data
    showAlert(name) {
      this.setState({ [name]: true });
    }
    hideAlert(name) {
      this.setState({ [name]: false });
    }

  // for diplaying file list
  handleUploadStart(fname, task) {
    this.state.dlProgress.set(task.snapshot.ref.fullPath, 0);
    if (!this.state.isUploading)
      this.setState({ isUploading: true, progress: 0 });
  }
  handleProgress(progress, task) {
    if (progress <= 100) {
      this.state.dlProgress.set(task.snapshot.ref.fullPath, progress)
      this.setState({ avatarURL: task.snapshot.ref.fullPath });
    }

  }
  handleUploadError(error) {
    this.setState({ isUploading: false });
    console.error(error);
  }
  handleUploadSuccess(filename, task) {
    // this.setState({avatar: filename, progress: 100, isUploading: false});
    this.state.dlProgress.delete(task.snapshot.ref.fullPath);
    if (this.state.dlProgress.size == 0)
      this.state.isUploading = false;

    firebase.app.storage().ref('meet_docs').child(filename).getDownloadURL()
      .then(url => {
        this.state.links.push(url);
        this.setState({ avatarURL: url });
      })
      .catch(error =>{
        alert("Error =>"+ error)
      })
  };

displayList() {
  var i;
  var items = [];
  for (i = 0; i < this.state.links.length; i++) {
    var link = this.state.links[i];
    var pts = link.split('/');
    var fn = unescape(pts[pts.length - 1].split('?')[0]);
    var fnparts = fn.split('/');
    items.push(<li>{fnparts[1]}</li>);
  }
  return items;
}

  //1. Enumerate all club IDs
  //2. For each club ID enumerate all athletes
  /**
   * Step 0a
   * Start getting data
   */
  componentDidMount() {
    this.findFirestorePath();
    window.sidebarInit();
  }

  /**
  * Step 0b
  * Resets data function
  */
  resetDataFunction() {
    var newState = {};
    newState.documents = [];
    newState.athletesByClub = [];
    newState.collections = [];
    newState.currentCollectionName = "";
    newState.fieldsAsArray = [];
    newState.arrayNames = [];
    newState.fields = [];
    newState.arrays = [];
    newState.elements = [];
    newState.elementsInArray = [];
    newState.theSubLink = null;
    newState.selectedCoach = "";
    newState.selectedOption = "";
    newState.to_club = "";
    newState.to_coach = "";
    newState.links = [];
    newState.note = "";

    this.setState(newState);
    this.findFirestorePath();
  }

  /**
   * Step 0c
   * componentWillReceiveProps event of React, fires when component is mounted and ready to display
   * Start connection to firebase
   */
  componentWillReceiveProps(nextProps, nextState) {
    console.log("Next SUB: " + nextProps.params.sub);
    console.log("Prev SUB : " + this.props.params.sub);
    if (nextProps.params.sub == this.props.params.sub) {
      console.log("update now");
      this.setState({ isLoading: true })
      this.resetDataFunction();
    }
  }

  /**
   * Step 0d
   * getMeTheFirestorePath created firestore path based on the router parh
   */
  getMeTheFirestorePath() {
    var thePath = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");;
    return thePath;
  }

  /**
   * Step 1
   * Finds out the Firestore path
   * Also creates the path that will be used to access the insert
   */
  findFirestorePath() {
    var pathData = {}
    if (this.props.params && this.props.params.sub) {
      pathData.lastSub = this.props.params.sub;
    }

    //Find the firestore path
    var firebasePath = this.getMeTheFirestorePath();
    pathData.firebasePath = firebasePath;
    if (firebasePath.includes("byClub") || firebasePath.includes("byGroup")) {
      var pathElems = firebasePath.split('/');
      pathData.grouping = pathElems.pop();
      pathData.firebasePath = pathElems.join('/');
    }

    //Find last path - the last item
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    pathData.lastPathItem = Common.capitalizeFirstLetter(items[items.length - 1]);
    pathData.completePath = subPath;

    //Save this in state
    this.setState(pathData);

    //Go to next step of finding the collection data
    this.getCollectionDataFromFireStore(pathData.firebasePath);
  }

  /**
  * Step 2
  * Connect to firestore to get the current item we need
  * @param {String} collection - this infact can be collection or document
  */
  async getCollectionDataFromFireStore(fPath) {

    //Create the segmments based on the path / collection we have
    var segments = fPath.split("/");
    var lastSegment = segments[segments.length - 1];

    //Is this a call to a collections data
    var isCollection = segments.length % 2;

    //first get the path to the clubs
    var collection = fPath;
    var entity = "";
    if (fPath.includes("{clubID}")) {
      collection = fPath.split('{')[0].slice(0, -1);
      entity = fPath.split('/').slice(-1)[0];
    }

    //Reference to this
    var _this = this;

    //Save know info for now
    this.setState({
      currentCollectionName: segments[segments.length - 1],
      isCollection: isCollection,
      isDocument: !isCollection,
    })

    //Get reference to firestore
    var db = firebase.app.firestore();

    //Here, we will save the documents from collection
    // var documents=[];
    var eDocsByClub = [];
    var workers = [];

    if (isCollection) {

      //COLLECTIONS - GET DOCUMENTS 

      db.collection(collection).orderBy("name").get()
        .then(function (querySnapshot) {

          querySnapshot.forEach(function (doc) {

            //Get the object
            var club = doc.data();
            //Sace uidOfFirebase inside him
            club.uidOfFirebase = doc.id;
            var clubName = club.name;

            //we are interested in the athletes in each club rather than the club itself
            const worker = db.collection(collection + "/" + doc.id + "/" + entity).orderBy("full_name").get();
            workers.push(worker);

            worker.then(function (athQuery) {

              var eDocs = [];

              athQuery.forEach(function (eDoc) {

                var currentDocument = eDoc.data();
                currentDocument.uidOfFirebase = eDoc.id;
                currentDocument.clubID = doc.id;
                currentDocument.Club_Name = clubName;

                //Save in the list of documents
                eDocs.push(currentDocument);

              });

              // alert("Pushing cname: "+clubName+" num docs: "+eDocs.length);
              clubName = clubName + " (" + eDocs.length + ")";
              eDocsByClub.push({ name: clubName, docs: eDocs });

            });

          });

          //Save the douments in the sate
          Promise.all(workers)
            .then(someval => {
              _this.setState({
                isLoading: false,
                athletesByClub: eDocsByClub,
                showAddCollection: ""
              })
            });

        });

    // to get club List
    var top_path = "aquaticsID/India/associations/KSA/clubs";
    var clubs = [];
    db.collection(top_path).get()
    .then(querySnapshot => {
      querySnapshot.forEach(function (doc) {
        var vals = doc.data();
        clubs.push({ value : doc.id , label : vals.name});
      })
      this.setState({ clubsList: clubs});
    })
    .catch(error => {
      console.log("Error getting coach list: ", error);
    })

    } else {
      //DOCUMENT - GET FIELDS && COLLECTIONS
      var referenceToCollection = collection.replace("/" + lastSegment, "");

      //Create reference to the document itseld
      var docRef = db.collection(referenceToCollection).doc(lastSegment);

      //Get the starting collectoin
      var parrentCollection = segments;
      parrentCollection.splice(-1, 1);

      //Find the collections of this document
      this.findDocumentCollections(parrentCollection);

      docRef.get().then(function (doc) {
        if (doc.exists) {
          console.log("Document data:", doc.data());

          //Directly process the data
          _this.processRecords(doc.data())
        } else {
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    }
  }

  /**
   * Step 3
   * findDocumentCollections - what collections should we display / currently there is no way to get collection form docuemnt
   * @param {Array} chunks - the collection / documents
   */
  findDocumentCollections(chunks) {
    console.log("Search for the schema now of " + chunks);

    //At start is the complete schema
    var theInsertSchemaObject = INSERT_STRUCTURE;
    var cuurrentFields = null;
    console.log("CHUNKS");
    console.log(chunks);

    //Foreach chunks, find the collections / fields
    chunks.map((item, index) => {
      console.log("current chunk:" + item);

      //Also make the last object any
      //In the process, check if we have each element in our schema
      if (theInsertSchemaObject != null && theInsertSchemaObject && theInsertSchemaObject[item] && theInsertSchemaObject[item]['collections']) {
        var isLastObject = (index == (chunks.length - 1));

        if (isLastObject && theInsertSchemaObject != null && theInsertSchemaObject[item] && theInsertSchemaObject[item]['fields']) {
          cuurrentFields = theInsertSchemaObject[item]['fields'];
        }

        if (isLastObject && theInsertSchemaObject != null) {
          //It is last
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        } else {
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        }
      } else {
        theInsertSchemaObject = [];
      }
      console.log("Current schema");
      console.log(theInsertSchemaObject);


    })

    //Save the collection to be shown as button and fieldsOfOnsert that will be used on inserting object
    this.setState({ collections: theInsertSchemaObject, fieldsOfOnsert: cuurrentFields })
  }

  /**
   * Step 4
   * Processes received records from firebase
   * @param {Object} records
   */
  processRecords(records) {
    console.log(records);

    var fields = {};
    var arrays = {};
    var elements = [];
    var elementsInArray = [];
    var newState = {};
    var directValue = "";
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.elements = elements;
    newState.directValue = directValue;
    newState.elementsInArray = elementsInArray;
    newState.records = null;

    this.setState(newState);

    //Each display is consisted of
    //Fields   - This are string, numbers, photos, dates etc...
    //Arrays   - Arrays of data, ex items:[0:{},1:{},2:{}...]
    //         - Or object with prefixes that match in array
    //Elements - Object that don't match in any prefix for Join - They are represented as buttons.

    //In FireStore
    //GeoPoint
    //DocumentReference

    //If record is of type array , then there is no need for parsing, just directly add the record in the arrays list
    if (Common.getClass(records) == "Array") {
      //Get the last name
      console.log("This is array");
      var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
      var allPathItems = subPath.split("+");
      console.log(allPathItems)
      if (allPathItems.length > 0) {
        var lastItem = allPathItems[allPathItems.length - 1];
        console.log(lastItem);
        arrays[lastItem] = records;

      }
      //this.setState({"arrays":this.state.arrays.push(records)})
    } else if (Common.getClass(records) == "Object") {
      //Parse the Object record
      for (var key in records) {
        if (records.hasOwnProperty(key)) {
          var currentElementClasss = Common.getClass(records[key]);
          console.log(key + "'s class is: " + currentElementClasss);

          //Add the items by their type
          if (currentElementClasss == "Array") {
            //Add it in the arrays  list
            arrays[key] = records[key];
          } else if (currentElementClasss == "Object") {
            //Add it in the elements list
            var isElementMentForTheArray = false; //Do we have to put this object in the array
            for (var i = 0; i < Config.adminConfig.prefixForJoin.length; i++) {
              if (key.indexOf(Config.adminConfig.prefixForJoin[i]) > -1) {
                isElementMentForTheArray = true;
                break;
              }
            }

            var objToInsert = records[key];
            objToInsert.uidOfFirebase = key;

            if (isElementMentForTheArray) {
              //Add this to the merged elements
              elementsInArray.push(objToInsert);
            } else {
              //Add just to elements
              elements.push(objToInsert);
            }

          } else if (currentElementClasss != "undefined" && currentElementClasss != "null") {
            //This is string, number, or Boolean
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "GeoPoint") {
            //This is GeoPOint
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "DocumentReference") {
            //This is DocumentReference
            //Add it to the fields list
            fields[key] = records[key];
          }

        }
      }
    } if (Common.getClass(records) == "String") {
      console.log("We have direct value of string");
      directValue = records;
    }

    //Convert fields from object to array
    var fieldsAsArray = [];
    console.log("Add the items now inside fieldsAsArray");
    console.log("Current schema");
    console.log(this.state.currentInsertStructure)
    //currentInsertStructure
    var keysFromFirebase = Object.keys(fields);
    console.log("keysFromFirebase")
    console.log(keysFromFirebase)
    var keysFromSchema = Object.keys(this.state.currentInsertStructure || {});
    console.log("keysFromSchema")
    console.log(keysFromSchema)

    keysFromSchema.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        var indexOfElementInFirebaseObject = keysFromFirebase.indexOf(key);
        if (indexOfElementInFirebaseObject > -1) {
          keysFromFirebase.splice(indexOfElementInFirebaseObject, 1);
        }
      }
    });

    console.log("keysFromFirebase")
    console.log(keysFromFirebase)

        //pp_add
        var navigation = Config.navigation;
        var itemFound = false;
        var showFields = null;
        for (var i = 0; i < navigation.length && !itemFound; i++) {
          if (navigation[i].editFields && navigation[i].link == "athlete") {
            showFields = navigation[i].editFields;
         
            itemFound = true;
          }
        }
        var pos = -1;
        //end pp_add
        keysFromFirebase.forEach((key) => {
          if (fields.hasOwnProperty(key)) {
            if (showFields)
              pos = showFields.indexOf(key);
            if (pos < 0)
              return;
            // fieldsAsArray.push({ "theKey": key, "value": fields[key] })
            // fieldsAsArray.splice(pos, 0, { "theKey": key, "value": fields[key] })
            fieldsAsArray[pos] =  { "theKey": key, "value": fields[key] }
          }
        });
        
    // keysFromFirebase.forEach((key) => {
    //   if (fields.hasOwnProperty(key)) {
    //     fieldsAsArray.push({ "theKey": key, "value": fields[key] })
    //   }
    // });



    //Get all array names
    var arrayNames = [];
    Object.keys(arrays).forEach((key) => {
      arrayNames.push(key)
    });

    var newState = {};
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.isJustArray = Common.getClass(records) == "Array";
    newState.elements = elements;
    newState.elementsInArray = elementsInArray;
    newState.directValue = directValue;
    newState.records = records;
    newState.isLoading = false;

    console.log("THE elements")
    console.log(elements);

    //Set the new state
    this.setState(newState);

    //Additional init, set the DataTime, check format if something goes wrong
    window.additionalInit();
  }

  /**
   *
   * Create R Update D
   *
   */

  /**
  * processValueToSave  - helper for saving in Firestore , converts value to correct format
  * @param {value} value
  * @param {type} type of field
  */
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }

  /**
  * updatePartOfObject  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {String} firebasePath current firestore path
  * @param {String} byGivvenSubLink force link to field
  * @param {Function} callback function after action
  */
  updatePartOfObject(key, value, dorefresh = false, type = null, firebasePath, byGivvenSubLink = null, callback = null) {
    var subLink = this.state.theSubLink;
    if (byGivvenSubLink != null) {
      subLink = byGivvenSubLink;
    }
    console.log("Sub save " + key + " to " + value + " and the path is " + firebasePath + " and theSubLink is " + subLink);
    var chunks = subLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    var _this = this;
    //First get the document
    //DOCUMENT - GET FIELDS && COLLECTIONS
    var docRef = firebase.app.firestore().doc(firebasePath);
    docRef.get().then(function (doc) {
      if (doc.exists) {
        var numChunks = chunks.length - 1;
        var doc = doc.data();
        if (value == "DELETE_VALUE") {
          if (numChunks == 2) {
            doc[chunks[1]].splice(chunks[2], 1);
          }
          if (numChunks == 1) {
            doc[chunks[1]] = null;
          }
        } else {
          //Normal update, or insert
          if (numChunks == 2) {
            doc[chunks[1]][chunks[2]] = value
          }
          if (numChunks == 1) {
            doc[chunks[1]][key] = value
          }
        }

        console.log("Document data:", doc);
        _this.updateAction(chunks[1], doc[chunks[1]], dorefresh, null, true)
        if (callback) {
          callback();
        }

        //alert(chunks.length-1);
        //_this.processRecords(doc.data())
        //console.log(doc);

      } else {
        console.log("No such document!");
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });

  }



  /**
  * Firebase update based on key / value,
  * This function also sets derect name and value
  * @param {String} key
  * @param {String} value
  */

  /**
  * updateAction  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {Boolean} forceObjectSave force saving sub object
  */
  updateAction(key, value, dorefresh = false, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/affiliate/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {
        //DO NOTHING: DISABLE EDITS IN PLACE
        this.setState({[key] : value});
        // var db = firebase.app.firestore();

        // var databaseRef = db.doc(firebasePath);
        // var updateObj={};
        // updateObj[key]=value;
        // databaseRef.set(updateObj, { merge: true });
      }

    }
  }

  /**
  * addDocumentToCollection  - used recursivly to add collection's document's collections
  * @param {String} name name of the collection
  * @param {FirestoreReference} reference
  */
  addDocumentToCollection(name, reference = null) {

    var pathChunks = this.state.firebasePath.split("/");
    pathChunks.pop();
    var withoutLast = pathChunks.join("/");
    console.log(name + " vs " + withoutLast);
    //Find the fields to be inserted
    var theInsertSchemaObject = INSERT_STRUCTURE[name].fields;
    console.log(JSON.stringify(theInsertSchemaObject));

    //Find the collections to be inserted
    var theInsertSchemaCollections = INSERT_STRUCTURE[name].collections;
    console.log(JSON.stringify(theInsertSchemaCollections));

    //Reference to root firestore or existing document reference
    var db = reference == null ? (pathChunks.length > 1 ? firebase.app.firestore().doc(withoutLast) : firebase.app.firestore()) : reference;

    //Check type of insert
    var isTimestamp = Config.adminConfig.methodOfInsertingNewObjects == "timestamp"

    //Create new element
    var newElementRef = isTimestamp ? db.collection(name).doc(Date.now()) : db.collection(name).doc()

    //Add data to the new element
    //newElementRef.set(theInsertSchemaObject)

    //Go over sub collection and insert them
    for (var i = 0; i < theInsertSchemaCollections.length; i++) {
      this.addDocumentToCollection(theInsertSchemaCollections[i], newElementRef)
    }


    //Show the notification on root element
    if (reference == null) {
      this.cancelAddFirstItem();
      this.setState({ notifications: [{ type: "success", content: "Element added. You can find it in the table bellow." }] });
      this.refreshDataAndHideNotification();
    }
  }

  /**
  * addKey
  * Adds key in our list of fields in firestore
  */
  addKey() {
    if (this.state.NAME_OF_THE_NEW_KEY && this.state.NAME_OF_THE_NEW_KEY.length > 0) {

      if (this.state.VALUE_OF_THE_NEW_KEY && this.state.VALUE_OF_THE_NEW_KEY.length > 0) {

        this.setState({ notifications: [{ type: "success", content: "New key added." }] });
        this.updateAction(this.state.NAME_OF_THE_NEW_KEY, this.state.VALUE_OF_THE_NEW_KEY);
        this.refs.simpleDialog.hide()
        this.refreshDataAndHideNotification();
      }
    }
  }

  /**
  * addItemToArray  - add item to array
  * @param {String} name name of the array
  * @param {Number} howLongItIs count of items, to know the next index
  */
  addItemToArray(name, howLongItIs) {
    console.log("Add item to array " + name);
    console.log("Is just array " + this.state.isJustArray);

    console.log("Data ");
    console.log(this.state.fieldsOfOnsert);

    var dataToInsert = null;
    var correctPathToInsertIn = "";
    if (this.state.fieldsOfOnsert) {
      if (this.state.isJustArray) {
        console.log("THIS IS Array")
        dataToInsert = this.state.fieldsOfOnsert[0];
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      } else {
        dataToInsert = this.state.fieldsOfOnsert[name];
        dataToInsert = dataToInsert ? dataToInsert[0] : null;
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + name + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      }
    }

    console.log("Data to insert");
    console.log(dataToInsert);
    console.log("Path to insert");
    console.log(correctPathToInsertIn);

    var _this = this;
    this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", dataToInsert, true, null, this.state.firebasePath, correctPathToInsertIn, function (e) {
      _this.setState({ notifications: [{ type: "success", content: "New element added." }] });
      _this.refreshDataAndHideNotification();
    })
  }

  /**
  *
  * C Read U D
  *
  */

  /**
  * showSubItems - displays sub object, mimics opening of new page
  * @param {String} theSubLink , direct link to the sub object
  */
  showSubItems(theSubLink) {
    var chunks = theSubLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    this.setState({
      itemOfInterest: chunks[1],
      theSubLink: theSubLink,
    })
    var items = this.state.records;
    for (var i = 1; i < chunks.length; i++) {
      console.log(chunks[i]);
      items = items[chunks[i]];
    }
    console.log("--- NEW ITEMS ");
    console.log(items)
    this.processRecords(items);
  }

  /**
  *
  * C R U Delete
  *
  */

  /**
  * deleteFieldAction - displays sub object, mimics opening of new page
  * @param {String} key to be updated
  * @param {Boolean} isItArrayItem 
  * @param {String} theLink 
  */
  deleteFieldAction(key, isItArrayItem = false, theLink = null) {
    console.log("Delete " + key);
    console.log(theLink);
    if (theLink != null) {
      theLink = theLink.replace("/affiliate", "");
    }
    if (isNaN(key)) {
      isItArrayItem = false;
    }
    console.log("Is it array: " + isItArrayItem);
    var firebasePathToDelete = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (key != null) {
      //firebasePathToDelete+=("/"+key)
    }

    console.log("firebasePath for delete:" + firebasePathToDelete);
    this.setState({ pathToDelete: theLink ? theLink : firebasePathToDelete, isItArrayItemToDelete: isItArrayItem, keyToDelete: theLink ? "" : key });
    window.scrollTo(0, 0);
    this.refs.deleteDialog.show();

  }
  moveAthleteAlert(Kreed_ID , theLink){
    var from_club = theLink.split('+')[5];
    this.state.fromClub = from_club;
    this.state.Kr_ID = Kreed_ID;   
    for(var i=0 ; i < this.state.athletesByClub.length ; i++){
      var docs = this.state.athletesByClub[i].docs;
        for(var j=0; j< docs.length; j++){
          if(docs[j].Kreed_ID == this.state.Kr_ID){
            this.state.moveAthleteName = docs[j].full_name;
          }
        }
    }
    this.showAlert("moveAthleteAlert");
  }
  closeMoveAthleteAlert(){
    this.hideAlert("moveAthleteAlert");
    this.state.selectedCoach = "";
    this.state.selectedOption = "";
    this.state.to_club = "";
    this.state.to_coach = "";
    this.state.links = [];
    this.state.note = "";
  }
  saveMoveAthleteAlert(){
    if(this.state.isUploading) {
      this.state.errorStatement ="Please wait! Files are uploading..."
      this.showAlert("documentsLoadingAlert");
      return;
    }

    if(this.state.to_club == "" || this.state.to_coach == "" ) {
      this.showAlert("changeAthleteErrorAlert");
      return;
    }

    if(this.state.links == "" ) {
      this.state.errorStatement ="Please upload your document"
      this.showAlert("documentsLoadingAlert");
      return;
    }
   
    this.hideAlert("moveAthleteAlert");
    this.moveAthlete(this.state.Kr_ID,this.state.fromClub,this.state.to_club,this.state.to_coach);
  }
  moveAthlete(krID, from, to , coach) {
    //  alert("KREED ID  :" + krID + "  FROM  :" + from + "  TO  :" + to + "  COACH  :" + coach)
    this.showAlert("movingAlert");

      var cfMoveAthlete = firebase.app.functions().httpsCallable('moveAthlete');
      cfMoveAthlete({kreedId: krID, fromClubId: from, toClubId: to, toCoach: coach })
      .then(result => {
        this.hideAlert("movingAlert");
        this.setState({ notifications: [{ type: "success", content: "Athlete Moved." }] })
        this.refreshDataAndHideNotification()
          // alert(result.data.message);
      });
  }
  //Activate Athlete
  activateAthleteAlert(Kreed_ID , activeStatus){ 
    this.state.activateKreedId = Kreed_ID;
    this.showAlert("activateAthleteAlert");
  }

  cancelActivateAthleteAlert(){
    this.hideAlert("activateAthleteAlert");
  }

  saveActivateAthleteAlert(){
    this.hideAlert("activateAthleteAlert");
    this.activateAthlete();
  }
  activateAthlete(){
    this.showAlert("movingAlert");
    var changeAthleteActivation = firebase.app.functions().httpsCallable('changeAthleteActivation');
    changeAthleteActivation({kreedId: this.state.activateKreedId, status: false})
    .then(result => {
      this.hideAlert("movingAlert");
      this.setState({ notifications: [{ type: "success", content: "Athlete Activated." }] })
      this.refreshDataAndHideNotification()
        // alert(result.data.message);
    });
  }

  // Deactivate athlete
  deactivateAthleteAlert(Kreed_ID , activeStatus){
    this.state.activateKreedId = Kreed_ID;
    this.showAlert("deactivateAthleteAlert");
  }

  cancelDeactivateAthleteAlert(){
    this.hideAlert("deactivateAthleteAlert");
  }
  saveDeactivateAthleteAlert(){
    this.hideAlert("deactivateAthleteAlert");
    this.deactivateAthlete();
  }
  deactivateAthlete(){
    this.showAlert("movingAlert");
    var changeAthleteActivation = firebase.app.functions().httpsCallable('changeAthleteActivation');
    changeAthleteActivation({kreedId: this.state.activateKreedId, status: true})
    .then(result => {
      this.hideAlert("movingAlert");
      this.setState({ notifications: [{ type: "success", content: "Athlete Deactivated." }] })
      this.refreshDataAndHideNotification()
    });
  }
  /**
  * doDelete - do the actual deleting based on the data in the state
  */
  doDelete() {
    var _this = this;
    console.log("Do delete ");
    console.log("Is it array " + this.state.isItArrayItemToDelete);
    console.log("Path to delete: " + this.state.pathToDelete)
    var completeDeletePath = this.state.pathToDelete + "/" + this.state.keyToDelete;
    console.log("completeDeletePath to delete: " + completeDeletePath)
    if (this.state.pathToDelete.indexOf(Config.adminConfig.urlSeparatorFirestoreSubArray) > -1) {
      //Sub data
      _this.refs.deleteDialog.hide();
      this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {
        _this.setState({ notifications: [{ type: "success", content: "Element deleted." }] });
        _this.refreshDataAndHideNotification();
      })
    } else {
      //Normal data

      var chunks = completeDeletePath.split("/");

      var db = firebase.app.firestore();


      if (chunks.length % 2) {
        //odd
        //Delete fields from docuemnt
        var refToDoc = db.doc(this.state.pathToDelete);

        // Remove the 'capital' field from the document
        var deleteAction = {};
        deleteAction[this.state.keyToDelete] = firebaseREF.firestore.FieldValue.delete();
        var removeKey = refToDoc.update(deleteAction).then(function () {
          console.log("Document successfully deleted!");
          _this.refs.deleteDialog.hide();
          _this.setState({ keyToDelete: null, pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });
      } else {
        //even
        //delete document from collection
        //alert("Delete document "+completeDeletePath);
        db.collection(this.state.pathToDelete).doc(this.state.keyToDelete).delete().then(function () {
          console.log("Document successfully deleted!");
          _this.refs.deleteDialog.hide();
          _this.setState({ pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });

      }

    }




    /*firebase.database().ref(this.state.pathToDelete).set(null).then((e)=>{
      console.log("Delete res: "+e)
      this.refs.deleteDialog.hide();
      this.setState({keyToDelete:null,pathToDelete:null,notifications:[{type:"success",content:"Field is deleted."}]});
      this.refreshDataAndHideNotification();

    })*/
  }

  /**
  * cancelDelete - user click on cancel
  */
  cancelDelete() {
    console.log("Cancel Delete");
    this.refs.deleteDialog.hide()
  }

  cancelAddFirstItem() {
    console.log("Cancel Add");
    this.refs.addCollectionDialog.hide()
  }



  /**
  *
  * UI GENERATORS
  *
  */

  /**
  * This function finds the headers for the current menu
  * @param firebasePath - we will use current firebasePath to find the current menu
  */
  findHeadersBasedOnPath(firebasePath) {
    var headers = null;
    var entity = firebasePath.split('/').slice(-1)[0];

    var itemFound = false;
    var navigation = Config.navigation;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].tableFields &&
        navigation[i].name == Common.capitalizeFirstLetter(entity)) {

        headers = navigation[i].tableFields;
        itemFound = true;
      }

      //Look into the sub menus
      if (navigation[i].subMenus) {
        for (var j = 0; j < navigation[i].subMenus.length; j++) {
          if (navigation[i].subMenus[j].path == firebasePath && navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "affiliate") {

            headers = navigation[i].subMenus[j].tableFields;
            itemFound = true;
          }
        }
      }
    }
    return headers;
  }

  /**
  * makeCollectionTable
  * Creates single collection documents
  */
  makeCollectionTable_not_used() {
    var name = this.state.currentCollectionName;

    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          {/* Removed code here that put up the add button */}

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">

              <MyTable
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={true}
                name={name}
                routerPath={this.props.route.path}
                isJustArray={false}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.documents}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
  // to get group of athlete based on age
  getGroup(dob) {
    var current = new Date();
    var today = current.getFullYear() - dob.getFullYear();
    if (today > 17) {
      return "Open";
    } else if (today == 17 || today == 16 || today == 15) {
      return "I";
    } else if (today == 14 || today == 13) {
      return "II";
    } else if (today == 12 || today == 11) {
      return "III";
    } else if (today == 10 || today == 9) {
      return "IV";
    } else if (today == 8 || today == 7) {
      return "V";
    } else if (today == 6 || today == 5) {
      return "VI";
    }
  }
  //to make table by group
  makeGroupTable() {
    var groups = { "I": [], "II": [], "III": [], "IV": [], "V": [], "VI": [], "Open": [] };
    var tables = [];

    for (var i = 0; i < this.state.athletesByClub.length; i++) {
      var cname = this.state.athletesByClub[i].name;
      var docs = this.state.athletesByClub[i].docs;

      if (docs.length < 1)
        continue;

      for (var j = 0; j < docs.length; j++)
        if (docs[j].date_of_birth && groups[this.getGroup(docs[j].date_of_birth)])
          groups[this.getGroup(docs[j].date_of_birth)].push(docs[j]);
    }

    for (var gID in groups) {

      var docs = groups[gID];
      var cname = "Group " + gID + " (" + docs.length + ")";

      if (docs.length < 1)
        continue;

      tables.push(
        <div className="col-md-12" key={cname}>


          <div className="material-datatables">
            <Accordion atomic={true}>
              <AccordionItem title={cname}>
                <MyTable
                  headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                  deleteFieldAction={this.deleteFieldAction}
                  fromObjectInArray={true}
                  name={name}
                  routerPath={this.props.route.path}
                  isJustArray={false}
                  sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                  data={docs}
                  moveAthleteAlert = {this.moveAthleteAlert}
                  deactivateAthleteAlert = {this.deactivateAthleteAlert}
                  activateAthleteAlert = {this.activateAthleteAlert}
                />
              </AccordionItem>
            </Accordion>
          </div>

        </div>
      );

    }
    tables.push(
      <div className="row margin-top-25px">
        <div className="col-sm-12 margin-left-25px ">
          <button onClick={this.export2csv} className="btn btn-rose font-size" value="Save"> <i className="material-icons">file_download</i> Export to csv</button>
        </div>
      </div>
    );

    return (tables);
  }

  //Export firestore data to CSV
  //24th May 2018
  export2csv() {
    if (this.state.athletesByClub.length < 1)
      return;

    var csvContent = "data:text/csv;charset=utf-8,";

    for (var i = 0; i < this.state.athletesByClub.length; i++) {
      var cname = this.state.athletesByClub[i].name;
      var docs = this.state.athletesByClub[i].docs;

      if (docs.length < 1)
        continue;

      for (var j = 0; j < docs.length; j++) {
        var row = [];
        row.push(docs[j].full_name,
          docs[j].gender,
          docs[j].date_of_birth,
          docs[j].swimGroup,
          docs[j].mobile_number,
          docs[j].email,
          docs[j].Kreed_ID,
          docs[j].KSA_ID,
          docs[j].coach_name,
          cname.split("(")[0]);

        csvContent += row.join();
        csvContent += "\r\n";
      }
    }

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "athletes_by_club.csv");
    document.body.appendChild(link); // Required for FF
    link.click();
    document.body.removeChild(link);
  }

  makeCollectionTable() {
    var name = this.state.currentCollectionName;
    var tables = [];

    if (this.state.athletesByClub.length < 1)
      return;

    if (this.state.grouping === "byGroup")
      return this.makeGroupTable();

    for (var i = 0; i < this.state.athletesByClub.length; i++) {

      var cname = this.state.athletesByClub[i].name;
      var docs = this.state.athletesByClub[i].docs;

      if (docs.length < 1)
        continue;

      tables.push(
        <div className="col-md-12" key={cname}>


          <div className="material-datatables">
            <Accordion atomic={true}>
              <AccordionItem title={cname}>
                <MyTable
                  headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                  deleteFieldAction={this.deleteFieldAction}
                  fromObjectInArray={true}
                  name={name}
                  routerPath={this.props.route.path}
                  isJustArray={false}
                  sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                  data={docs}
                  moveAthleteAlert = {this.moveAthleteAlert}
                  deactivateAthleteAlert = {this.deactivateAthleteAlert}
                  activateAthleteAlert = {this.activateAthleteAlert}
                />
              </AccordionItem>
            </Accordion>
          </div>

        </div>
      );


    }
    tables.push(
      <div className="row margin-top-25px">
        <div className="col-sm-12 margin-left-25px ">
          <button onClick={this.export2csv} className="btn btn-rose font-size" value="Save"> <i className="material-icons">file_download</i> Export to csv</button>
        </div>
      </div>
    );

    return (tables);
  }

  /**
   * Creates single array section
   * @param {String} name, used as key also
   */
  makeArrayCard(name) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <DocsTable
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                //headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                headers={["name"]}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={false} name={name}
                routerPath={this.props.route.path}
                isJustArray={this.state.isJustArray}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.arrays[name]} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates  table section for the elements object
   * @param {String} name, used as key also
   */
  makeTableCardForElementsInArray() {
    var name = this.state.lastPathItem;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)} deleteFieldAction={this.deleteFieldAction} fromObjectInArray={true} name={name} routerPath={this.props.route.path} isJustArray={this.state.isJustArray} sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""} data={this.state.elementsInArray}>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
    * Creates direct value section
    * @param {String} value, valu of the current path
    */
  makeValueCard(value) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <div className="card-content">
            <h4 className="card-title">Value</h4>
            <div className="toolbar">
            </div>
            <div>
              <Input updateAction={this.updateAction} className="" theKey="DIRECT_VALUE_OF_CURRENT_PATH" value={value} />
            </div>
          </div>
        </div>
      </div>
    )
  }


  /**
   * generateBreadCrumb
   */
  generateBreadCrumb() {
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    var path = "/affiliate/"
    return (<div>{items.map((item, index) => {
      if (index == 0) {
        path += item;
      } else {
        path += "+" + item;
      }

      return (<Link className="navbar-brand" to={path}>{item} <span className="breadcrumbSeparator">{index == items.length - 1 ? "" : "/"}</span><div className="ripple-container"></div></Link>)
    })}</div>)
  }

  /**
   * generateNotifications
   * @param {Object} item - notification to be created
   */
  generateNotifications(item) {
    return (
      <div className="col-md-12">
        <Notification type={item.type} >{item.content}</Notification>
      </div>
    )
  }

  /**
  * refreshDataAndHideNotification
  * @param {Boolean} refreshData 
  * @param {Number} time 
  */
  refreshDataAndHideNotification(refreshData = true, time = 3000) {
    //Refresh data,
    if (refreshData) {
      this.resetDataFunction();
    }

    //Hide notifications
    setTimeout(function () { this.setState({ notifications: [] }) }.bind(this), time);
  }

  //MAIN RENDER FUNCTION 
  render() {
    const { selectedOption } = this.state;
    const value = selectedOption && selectedOption.value;
    
    const { selectedCoach } = this.state;
  	const valueCoach = selectedCoach && selectedCoach.value;
    
    //styling for add new athlete skylight
    var addnewitemDialog = {
      width: '70%',
      height: 'auto',
      marginLeft: '-35%',
      position: 'absolute'
    };
    return (
      <div className="content">
        <NavBar>
          <div className="pull-right">
            <a href="http://karnatakaswimming.org" target="_blank"> <img className="img-circle" src='assets/img/ksa_logo.png' height="70" width="80" /></a>
          </div>
        </NavBar>

        <div className="content" sub={this.state.lastSub}>

          <div className="container-fluid">

            <div style={{ textAlign: 'center' }}>
              {/* LOADER */}
              {this.state.isLoading ? <Loader color="#8637AD" size="12px" margin="4px" /> : ""}
            </div>

            {/* NOTIFICATIONS */}
            {this.state.notifications ? this.state.notifications.map((notification) => {
              return this.generateNotifications(notification)
            }) : ""}

             {/* SWEET ALERTS */}
             <SweetAlert
              showCancel  
              show={this.state.moveAthleteAlert}
              confirmBtnCssClass="btn btn-rose btn-size font-size"
              cancelBtnCssClass="btn btn-rose btn-size font-size"
              onConfirm={this.saveMoveAthleteAlert}
              onCancel={this.closeMoveAthleteAlert}>
              <div className="card-content ">
                <div className="card display-inline">
                  <div className="card-header card-header-icon margin-tl" data-background-color="rose">
                    <i className="material-icons">shuffle</i>
                  </div>
                </div>
              </div>
             <div> <h4 className="title-move-styling">Move {this.state.moveAthleteName}</h4></div>
              <p className="move-styling">Select Club</p>
              <Select
                name="form-field-name"
                value={value}
                onChange={this.handleChange}
                options={this.state.clubsList}
                clearable={true}
                searchable={true}
              />
              <p className="move-styling">Assign Coach <Indicator show={this.state.isLoadingCoaches} /></p>    
              <Select
                name="form-field-name"
                value={valueCoach}
                onChange={this.handleCoachChange}
                options={this.state.clubCoaches}
                clearable={true}
                searchable={true}
              />

              <p className="move-styling">Attachments </p>
              <ol className="ol-styling">
                {this.displayList()}
              </ol>
              <label style={{ backgroundColor: '#E82062', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor', marginTop: '5px' }}>
                Select Files
                <FileUploader
                  hidden
                  accept=".gif, .jpg, .png, .pdf, .doc, .docx"
                  name="attachments"
                  multiple
                  storageRef={firebase.app.storage().ref('meet_docs')}
                  onUploadStart={this.handleUploadStart}
                  onUploadError={this.handleUploadError}
                  onUploadSuccess={this.handleUploadSuccess}
                  onProgress={this.handleProgress}
                />
              </label>
              <Indicator show={this.state.isUploading} />
              <p className="move-styling">Note </p>
              <TextArea className="border-styling" theKey="note" updateAction={this.updateAction} value={this.state.note}/>


              
              
            </SweetAlert>

            {/* SWEET ALERTS */}
              <SweetAlert warning
              showCancel  
              show={this.state.activateAthleteAlert}
              confirmBtnCssClass="btn btn-rose btn-size font-size"
              cancelBtnCssClass="btn btn-rose btn-size font-size"
              onConfirm={this.saveActivateAthleteAlert}
              onCancel={this.cancelActivateAthleteAlert}>
              Are you sure you want to Active the athlete?
            </SweetAlert>

              <SweetAlert warning
              showCancel  
              show={this.state.deactivateAthleteAlert}
              confirmBtnCssClass="btn btn-rose btn-size font-size"
              cancelBtnCssClass="btn btn-rose btn-size font-size"
              onConfirm={this.saveDeactivateAthleteAlert}
              onCancel={this.cancelDeactivateAthleteAlert}>
              Are you sure you want to Deactive the athlete?
            </SweetAlert>

             {/* sweet alert */}
             <SweetAlert warning
              show={this.state.changeAthleteErrorAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ changeAthleteErrorAlert: false })}>
              Please ensure you have selected a club and a coach.
              </SweetAlert>

              {/* SWEET ALERTS */}

              {/* sweet alert */}
             <SweetAlert warning
              show={this.state.documentsLoadingAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ documentsLoadingAlert: false })}>
               {this.state.errorStatement}
              </SweetAlert>

              {/* SWEET ALERTS */}
            <SweetAlert
              show={this.state.movingAlert}
              title=" Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
            </SweetAlert>

            {/* Documents in collection */}
            {this.state.isCollection ? this.makeCollectionTable() : ""}

            {/* DIRECT VALUE */}
            {this.state.directValue && this.state.directValue.length > 0 ? this.makeValueCard(this.state.directValue) : ""}


            {/* COLLECTIONS */}
            {this.state.theSubLink == null && this.state.isDocument && this.state.collections && this.state.collections.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{"Collections"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.theSubLink == null && this.state.collections ? this.state.collections.map((item) => {
                      var theLink = "/affiliate/" + this.state.completePath + Config.adminConfig.urlSeparator + item;
                      return (<Link to={theLink}><a className="btn">{item}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}

            {/* ARRAYS */}
            {this.state.arrayNames ? this.state.arrayNames.map((key) => {
              return this.makeArrayCard(key)
            }) : ""}

            {/* FIELDS */}
            {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? (<div className="col-md-12">
              <div className="card">
                {/*
                <a  onClick={()=>{this.refs.simpleDialog.show()}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}>
                    <i className="material-icons">add</i>
                </div></a>*/}
                <form className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{Common.capitalizeFirstLetter(Config.adminConfig.fieldBoxName)}</h4>
                  </div>
                  {this.state.fieldsAsArray ? this.state.fieldsAsArray.map((item) => {

                    return (
                      <Fields
                        isFirestore={true}
                        parentKey={null}
                        key={item.theKey + this.state.lastSub}
                        deleteFieldAction={this.deleteFieldAction}
                        updateAction={this.updateAction}
                        theKey={item.theKey}
                        value={item.value} />)


                  }) : ""}
                  <div
                    className="text-center">

                    <Link to={'/athlete/aquaticsID+India+associations+KSA+clubs+{clubID}+athletes+' + this.state.grouping}>
                      <button
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Cancel">Close</button>
                    </Link>

                  </div>

                </form>
              </div>
            </div>) : ""}

            {/* ELEMENTS MERGED IN ARRAY */}
            {this.state.elementsInArray && this.state.elementsInArray.length > 0 ? (this.makeTableCardForElementsInArray()) : ""}

            {/* ELEMENTS */}
            {this.state.elements && this.state.elements.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{this.state.lastPathItem + "' elements"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.elements ? this.state.elements.map((item) => {
                      var theLink = "/fireadmin/" + this.state.completePath + Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
                      return (<Link onClick={() => { this.showSubItems(theLink) }}><a className="btn">{item.uidOfFirebase}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}




          </div>
        </div>
        {/* Skylight to delete a item */}
        <SkyLight hideOnOverlayClicked ref="deleteDialog" title="">
          <span><h3 className="center-block">Delete data</h3></span>
          <div className="col-md-12">
            <Notification type="danger" >All data at this location, but not nested collections, will be deleted! To delete any collection's data go in each collection and detele the documents</Notification>
          </div>
          {/* <div className="col-md-12">
              Data Location
          </div>
          <div className="col-md-12">
              <b>{this.state.pathToDelete+"/"+this.state.keyToDelete}</b>
          </div> */}

          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelDelete} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.doDelete} className="btn btn-danger center-block">Delete</a>
            </div>

          </div>

        </SkyLight>
        {/* Skylight to delete a item */}
        {/* Skylight for adding first athelete */}
        <SkyLight hideOnOverlayClicked ref="addCollectionDialog" title="">
          <span><h3 className="center-block">Add first document in collection</h3></span>
          <div className="col-md-12">
            <Notification type="success" >Looks like there are no documents in this collection. Add your first document in this collection</Notification>
          </div>

          {/* <div className="col-md-12">
              Data Location
          </div>
          <div className="col-md-12">
              <b>{this.state.showAddCollection}</b>
          </div> */}


          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelAddFirstItem} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={() => { this.addDocumentToCollection(this.state.currentCollectionName) }} className="btn btn-success center-block">ADD</a>
            </div>

          </div>

        </SkyLight>
        {/*End Skylight for adding first athelete */}
{/* Skylight for adding new athelete */}
        {/*begin pp_add*/}
        {/* <SkyLight dialogStyles={addnewitemDialog} hideOnOverlayClicked ref="addnewitemDialog">
          <h2 className="text-align-center">Add New Club</h2>
          <div className="card-content">
            <form>
              <div className="row margin-tb-15">
                <div className="col-sm-3 text-align-right">
                  <label className="control-label label-color" for="name">Name :</label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <Input id="name" type="text" className="" />
                </div>
              </div>
              <div className="row margin-tb-15">
                <div className="col-sm-3 text-align-right">
                  <label className="control-label label-color" for="address">Address :</label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <Input id="address" type="text" className="" />
                </div>
              </div>
              <div className="row margin-tb-15">
                <div className="col-sm-3 text-align-right">
                  <label className="control-label label-color" for="contact_name">Contact Name :</label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <Input id="contact_name" type="text" className="" />
                </div>
              </div>
              <div className="row margin-tb-15">
                <div className="col-sm-3 text-align-right">
                  <label className="control-label label-color" for="email">E-Mail :</label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <Input id="email" type="email" className="" />
                </div>
              </div>
              <div className="row margin-tb-15">
                <div className="col-sm-3 text-align-right">
                  <label className="control-label label-color" for="mobile_number">Mobile Number :</label>
                </div>
                <div className="col-sm-8 margin-padding-0">
                  <Input id="mobile_number" type="number" className="" />
                </div>
              </div>
              <div className="row margin-top-25px">
                <div className="col-sm-12 text-align-center">
                  <button className="btn btn-rose font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                </div>
              </div>
            </form>
          </div>
        </SkyLight> */}
        {/*end pp_add*/}
{/*End Skylight for adding first athelete */}

        <SkyLight hideOnOverlayClicked ref="simpleDialog" title="">
          <span><h3 className="center-block">Add new key</h3></span>
          <br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Name of they key</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="NAME_OF_THE_NEW_KEY" value={"name"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div><br /><br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Value</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="VALUE_OF_THE_NEW_KEY" value={"value"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div>
          <div className="col-sm-12 ">
            <div className="col-sm-3 ">
            </div>
            <div className="col-sm-6 center-block">
              <a onClick={this.addKey} className="btn btn-rose btn-round center-block"><i className="fa fa-save"></i>   Add key</a>
            </div>
            <div className="col-sm-3 ">
            </div>
          </div>
        </SkyLight>
      </div>
    )
  }

}
export default AthleteList;

