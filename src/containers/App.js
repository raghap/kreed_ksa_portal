import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import NavBar from '../components/NavBar'
import Input from '../components/fields/Input.js';
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
require("firebase/firestore");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      num_clubs: '',
      num_athletes: '',
      num_coaches: '',
      num_meets: ''
    }
    this.getData = this.getData.bind(this);
    // this.displayathelets = this.displayathelets.bind(this);
  }
  //to get data from firebase db
  getData() {
    var db = firebase.app.firestore();
    var _this = this;

    db.doc("aquaticsID/India/associations/KSA").get().then(
      function (snap) {
        _this.setState({
          num_clubs: snap.data().num_clubs,
          num_athletes: snap.data().num_athletes,
          num_coaches: snap.data().num_coaches,
          num_meets: snap.data().num_meets
        });

      }
    )
  }

  componentDidMount() {
    this.getData();
    window.sidebarInit();
    //Uncomment if you want to do a edirect
    //this.props.router.push('/fireadmin/clubs+skopje+items') //Path where you want user to be redirected initialy
  }
  render() {
    return (
      <div className="content">
        <NavBar>
          <div>
            <a className="navbar-brand dashboard">Dashboard</a>
            <div className="pull-right">
              <a href="http://karnatakaswimming.org" target="_blank"> <img className="img-circle" src='assets/img/ksa_logo.png' height="70" width="80" /></a>
            </div>

            {/* <a className="navbar-brand">Dashboard</a>
           
            <ul className="nav navbar-nav navbar-right ">
              <li><div className="margin-padding-0">
               <Input placeholder="Search..." id="search" type="text" className=""/>
              </div>
              </li>
              <li><a href="#"><i className="material-icons">search</i></a></li>
              <li><a href="#"><i className="material-icons">dashboard</i></a></li>
              <li><a href="#"><i className="material-icons">notifications</i></a></li>
              <li><a href="http://karnatakaswimming.org" target="_blank"><img className="img-circle" src='assets/img/ksa_logo.png'  height="70" width="80"  /></a></li>
            </ul> */}

          </div>
        </NavBar>
        <div className="row margin-top-70">
          {/* number of Clubs */}
          <Link to="/assoc/aquaticsID+India+associations+KSA+clubs">
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">location_on</i>
                </div>
                <div className="text-align-right padding-10">
                  <p className="card-category">Clubs</p>
                  <h3 className="card-title">{this.state.num_clubs}</h3>
                </div>
              </div>
            </div>
          </Link>
          {/* number of Clubs */}
          {/* number of Coaches */}
          <Link to="/affiliate/aquaticsID+India+associations+KSA+clubs+{clubID}+coaches">
            <div className="col-lg-3 col-md-6 col-sm-6" >
              <div className="card card-stats">
                <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">accessibility</i>
                </div>
                <div className="text-align-right padding-10">
                  <p className="card-category">Coaches</p>
                  <h3 className="card-title">{this.state.num_coaches}</h3>
                </div>
              </div>
            </div>
          </Link>
          {/* number of Coaches */}
          {/* number of Athletes */}
          <Link to="/athlete/aquaticsID+India+associations+KSA+clubs+{clubID}+athletes+byClub">
            <div className="col-lg-3 col-md-6 col-sm-6" >
              <div className="card card-stats">
                <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">pool</i>
                </div>
                <div className="text-align-right padding-10">
                  <p className="card-category">Athletes</p>
                  <h3 className="card-title">{this.state.num_athletes}</h3>
                  {/* <p>Swimming: 10, Diving: 10, Water Polo: 10</p> */}
                </div>
              </div>
            </div>
          </Link>
           {/* number of Athletes */}
            {/* number of Meets */}
            <Link to="/meets/aquaticsID+India+associations+KSA+meets">
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">alarm</i>
                </div>
                <div className="text-align-right padding-10">
                  <p className="card-category">Meets</p>
                  <h3 className="card-title">{this.state.num_meets}</h3>
                </div>
              </div>
            </div>
          </Link>
          {/* number of Meets */}
        </div>

       {/* <div className="row margin-top-70"> */}
          {/* number of Meets */}
          {/* <Link to="/meets/aquaticsID+India+associations+KSA+meets">
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">alarm</i>
                </div>
                <div className="text-align-right padding-10">
                  <p className="card-category">Meets</p>
                  <h3 className="card-title">{this.state.num_meets}</h3>
                </div>
              </div>
            </div>
          </Link> */}
          {/* number of Clubs */}
        {/* </div>         */}

      </div>
    )
  }
}
export default App;
