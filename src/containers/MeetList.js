import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom';
import { Link } from 'react-router'
import firebase from '../config/database'
import Fields from '../components/fields/Fields.js'
import Input from '../components/fields/Input.js';
import Image from '../components/fields/Image.js';
import Table from '../components/tables/Table.js';
import SimpleTable from '../components/tables/SimpleTable.js';
import MeetDocsTable from '../components/tables/MeetDocsTable.js'
import FileUploader from 'react-firebase-file-uploader';
import Indicator from '../components/Indicator'

import 'babel-polyfill';
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';

import Config from '../config/app';
import Common from '../common.js';
import Notification from '../components/Notification';
import SkyLight from 'react-skylight';
import INSERT_STRUCTURE from "../config/firestoreschema.js"
import FirebasePaginator from "firebase-paginator"
import NavBar from '../components/NavBar'
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';
import DateTime from '../components/fields/DateTime.js';
import Select from '../components/fields/Select.js';
import SelectVenue from '../components/fields/SelectVenue.js';
import * as firebaseREF from 'firebase';
import { TextArea } from '../components/fields';
require("firebase/firestore");
// for file uploader
const uuidv1 = require('uuid/v1');

const ROUTER_PATH = "/meets/";
var Loader = require('halogen/PulseLoader');

class MeetList extends Component {

  constructor(props) {
    super(props);

    //Create initial step
    this.state = {
      meets: [],
      sessions: [],
      events: [],
      documents: [],
      collections: [],
      currentCollectionName: "",
      isCollection: false,
      isDocument: false,
      keyToDelete: null,
      pathToDelete: null,
      theSubLink: null,
      fieldsOfOnsert: null,
      isLoading: true,
      swapLoading: false,
      showAddCollection: "",

      debounce: false,
      imageLoading: false,
      meet_name: "",
      from: "",
      to: "",
      course: "Short(25m)",
      other_course: "",
      venue: "",
      max_events: "",
      description: "",
      links:[],
      avatarURL:"",
      isUploading:false,
      dlProgress: new Map(),

      country: "India",
      session_name: "",

      distance: "100m",
      stroke: "Free Style",
      category: "Boys/Men",
      swim_group: "I",
      event_number: "",

      editedFields: [],
      errors: {},


      itemToDelete: "event",
      phoneAlert: false,
      loadingAlert: false,
      deleteAlert: false,
      dateAlert: false,
      closeMeetAlert: false,
      links: [],
      isItArrayItemToDelete: false,

    };

    //Bind function to this
    this.getCollectionDataFromFireStore = this.getCollectionDataFromFireStore.bind(this);
    this.getMeets = this.getMeets.bind(this);
    this.resetDataFunction = this.resetDataFunction.bind(this);
    this.processRecords = this.processRecords.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
    this.cancelAddFirstItem = this.cancelAddFirstItem.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.deleteFieldAction = this.deleteFieldAction.bind(this);
    this.refreshDataAndHideNotification = this.refreshDataAndHideNotification.bind(this);
    this.addKey = this.addKey.bind(this);
    this.showSubItems = this.showSubItems.bind(this);
    this.updatePartOfObject = this.updatePartOfObject.bind(this);
    this.addDocumentToCollection = this.addDocumentToCollection.bind(this);
    this.addItemToArray = this.addItemToArray.bind(this);
    this.cancelAddNewMeet = this.cancelAddNewMeet.bind(this);
    this.addNewMeet = this.addNewMeet.bind(this);
    //add new session
    this.addNewSession = this.addNewSession.bind(this);
    this.cancelAddNewSession = this.cancelAddNewSession.bind(this);
    // add new event
    this.addNewEvent = this.addNewEvent.bind(this);
    this.cancelAddNewEvent = this.cancelAddNewEvent.bind(this);
    // this.formValueCapture=this.formValueCapture.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.validateInputs = this.validateInputs.bind(this);
    this.saveEdits = this.saveEdits.bind(this);
    this.resetEdits = this.resetEdits.bind(this);
    this.markUploaderStart = this.markUploaderStart.bind(this);

    //Alerts related
    this.hideAlert = this.hideAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);

    // for hiding add new meet skylight
    this.openAddNewMeet = this.openAddNewMeet.bind(this);
    this.listMeets = this.listMeets.bind(this);

    this.addSessionBtnHandler = this.addSessionBtnHandler.bind(this);
    this.addEventBtnHandler = this.addEventBtnHandler.bind(this);
    this.refreshEvents = this.refreshEvents.bind(this);

    this.deleteMeet = this.deleteMeet.bind(this);
    this.editMeet = this.editMeet.bind(this);

    //to close a meet
    this.closeMeet = this.closeMeet.bind(this);
    this.confirmCloseMeet = this.confirmCloseMeet.bind(this);
    this.cancelCloseMeet = this.cancelCloseMeet.bind(this);

    this.deleteSession = this.deleteSession.bind(this);
    this.cancelAlertDelete = this.cancelAlertDelete.bind(this);
    //to swap events
    this.swapSequence = this.swapSequence.bind(this);

    //documents
    this.displayList = this.displayList.bind(this);
    this.handleUploadStart = this.handleUploadStart.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleUploadError = this.handleUploadError.bind(this);
    this.handleUploadSuccess = this.handleUploadSuccess.bind(this);

    //documents update
    this.updateAttachments = this.updateAttachments.bind(this);
    this.cancelAttachments = this.cancelAttachments.bind(this);
   
  }
    // for diplaying file list
    handleUploadStart(fname, task) {
      this.state.dlProgress.set(task.snapshot.ref.fullPath, 0);
      if (!this.state.isUploading)
        this.setState({ isUploading: true, progress: 0 });
    }
    handleProgress(progress, task) {
      if (progress <= 100) {
        this.state.dlProgress.set(task.snapshot.ref.fullPath, progress)
        this.setState({ avatarURL: task.snapshot.ref.fullPath });
      }
  
    }
    handleUploadError(error) {
      this.setState({ isUploading: false });
      console.error(error);
    }
    handleUploadSuccess(filename, task) {
      // this.setState({avatar: filename, progress: 100, isUploading: false});
      this.state.dlProgress.delete(task.snapshot.ref.fullPath);
      if (this.state.dlProgress.size == 0)
        this.state.isUploading = false;

      firebase.app.storage().ref('meet_docs').child(filename).getDownloadURL()
        .then(url => {
          this.state.links.push(url);
          this.setState({ avatarURL: url });
        })
        .catch(error =>{
          alert("Error =>"+ error)
        })
    };

  displayList() {
    var i;
    var items = [];
    for (i = 0; i < this.state.links.length; i++) {
      var link = this.state.links[i];
      var pts = link.split('/');
      var fn = unescape(pts[pts.length - 1].split('?')[0]);
      var fnparts = fn.split('/');
      items.push(<li>{fnparts[1]}</li>);
    }
    return items;
  }
  cancelAlertDelete() {
    this.setState({ deleteAlert: false });
    this.state.itemToDelete = "event";
  }
  openAddNewMeet() {
    this.refs.addnewitemDialog.show();
    this.refs.addCollectionDialog.hide();
  }
  //sweet alerts for delete data
  showAlert(name) {
    this.setState({ [name]: true });
  }
  hideAlert(name) {
    this.setState({ [name]: false });
  }

  /**
   * Step 0a
   * Start getting data
   */
  componentDidMount() {
    this.findFirestorePath();
    window.sidebarInit();
  }

  /**
  * Step 0b
  * Resets data function
  */
  resetDataFunction() {
    var newState = {};
    newState.documents = [];
    newState.collections = [];
    newState.currentCollectionName = "";
    newState.fieldsAsArray = [];
    newState.arrayNames = [];
    newState.fields = [];
    newState.arrays = [];
    newState.elements = [];
    newState.elementsInArray = [];
    newState.theSubLink = null;

    newState.meet_name = "";
    newState.venue = "";
    newState.from = "";
    newState.to = "";
    newState.course = "Short(25m)";
    newState.other_course = "";
    newState.max_events = "";
    newState.description = "";
    newState.links = [];
    this.state.dlProgress.clear();
    newState.session_name = "";
 

    newState.distance = "100m";
    newState.stroke = "Free Style";
    newState.category = "Boys/Men";
    newState.swim_group = "I";
    newState.event_number = "";
    //newState.editedFields = [];
    this.resetEdits();

    this.setState(newState);
    this.findFirestorePath();
  }

  /**
   * Step 0c
   * componentWillReceiveProps event of React, fires when component is mounted and ready to display
   * Start connection to firebase
   */
  componentWillReceiveProps(nextProps, nextState) {
    console.log("Next SUB: " + nextProps.params.sub);
    console.log("Prev SUB : " + this.props.params.sub);
    if (nextProps.params.sub == this.props.params.sub) {
      console.log("update now");
      this.setState({ isLoading: true })
      this.resetDataFunction();
    }
  }

  /**
   * Step 0d
   * getMeTheFirestorePath created firestore path based on the router parh
   */
  getMeTheFirestorePath() {
    var thePath = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");;
    return thePath;
  }

  /**
   * Step 1
   * Finds out the Firestore path
   * Also creates the path that will be used to access the insert
   */
  findFirestorePath() {
    var pathData = {}
    if (this.props.params && this.props.params.sub) {
      pathData.lastSub = this.props.params.sub;
    }

    //Find the firestore path
    var firebasePath = this.getMeTheFirestorePath();
    pathData.firebasePath = firebasePath;

    //Find last path - the last item
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    pathData.lastPathItem = Common.capitalizeFirstLetter(items[items.length - 1]);
    pathData.completePath = subPath;

    //Save this in state
    this.setState(pathData);

  
    //Go to next step of finding the collection data
    this.getCollectionDataFromFireStore(firebasePath);
       //-----Block of code to get the list of coaches in this club
       var venues = [];
       var db = firebase.app.firestore();
       pathData.isLoading = true;
       var ccoll = pathData.firebasePath.replace(/\/[^\/]*$/, '/venues');
       db.collection(ccoll).get()
         .then(querySnapshot => {
           querySnapshot.forEach(function (doc) {
             var vals = doc.data();
             venues.push(vals.name);
           })
           this.setState({ meetVenues: venues });
         })
         .catch(error => {
           console.log("Error getting coach list: ", error);
         })
    //this.getMeets();
  }

  swapSequence(event1, event2) {
    this.setState({ swapLoading: true });
    var eparts = event1.split('/');
    const m_id = eparts[0];
    const s_id = eparts[2];

    const dbpath = "aquaticsID/India/associations/KSA/meets/";
    var seq1, seq2;

    var db = firebase.app.firestore();

    var docref1 = db.doc(dbpath + event1);
    var docref2 = db.doc(dbpath + event2);

    docref1.get()
      .then(snap1 => {
        seq1 = snap1.data().sequence;
        return docref2.get();
      })
      .then(snap2 => {
        seq2 = snap2.data().sequence;
        return docref1.update({ sequence: seq2 });
      })
      .then(someval => {
        return docref2.update({ sequence: seq1 });
      })
      .then(someval => {
        this.refreshEvents(m_id, s_id, false);
      })
      .catch(err => {
        console.log("Error in swap: " + err);
      })

  }

  getMeets() {
    var db = firebase.app.firestore();
    var top_path = "aquaticsID/India/associations/KSA/meets";

    var lmeets = [];
    var lsessions = [];
    var levents = [];
    var tasks = [], etasks = [];

    var _this = this;

    db.collection(top_path).orderBy("from").get()
      .then(function (meet_snap) {

        meet_snap.forEach(function (meet_doc) {
          var doc = meet_doc.data();
          doc.meet_id = meet_doc.id;
          lmeets.push({ id: meet_doc.id, data: doc });

          tasks.push(db.collection(top_path + "/" + meet_doc.id + "/sessions").get());
        });
      })
      .then(someval => {

        for (var subtask of tasks) {

          subtask.then(function (session_snap) {
            session_snap.forEach(function (session_doc) {

              var sdoc = session_doc.data();

              sdoc.session_id = session_doc.id;
              sdoc.meet_id = session_doc.ref.parent.parent.id;
              lsessions.push({ id: session_doc.id, data: sdoc });

              etasks.push(db.collection(top_path + "/" + sdoc.meet_id + "/sessions/" + session_doc.id + "/events").orderBy("sequence").get());

            });
          });
        }
        return Promise.all(tasks);
      })
      .then(someval => {

        for (var etask of etasks) {
          etask.then(function (event_snap) {
            event_snap.forEach(function (event_doc) {
              var edoc = event_doc.data();
              edoc.event_id = event_doc.id;
              edoc.session_id = event_doc.ref.parent.parent.id;
              edoc.meet_id = event_doc.ref.parent.parent.parent.parent.id;
              edoc.uidOfFirebase = edoc.meet_id + "+sessions+" + edoc.session_id + "+events+" + edoc.event_id;
              levents.push({ id: event_doc.id, data: edoc });
            });

          });
        }

        return Promise.all(etasks);
      })
      .then(someval => {
        // alert("sessions: "+lsessions.length+" events: "+levents.length);
        _this.setState({ isLoading: false, isCollection: true, meets: lmeets, sessions: lsessions, events: levents });
      });

  }
  /**
  * Step 2
  * Connect to firestore to get the current item we need
  * @param {String} collection - this infact can be collection or document
  */
  getCollectionDataFromFireStore(collection) {
    //Create the segmments based on the path / collection we have
    var segments = collection.split("/");
    var lastSegment = segments[segments.length - 1];

    //Is this a call to a collections data
    var isCollection = segments.length % 2;

    //Reference to this
    var _this = this;

    //Save know info for now
    this.setState({
      currentCollectionName: segments[segments.length - 1],
      isCollection: isCollection,
      isDocument: !isCollection,
    })

    //Get reference to firestore
    var db = firebase.app.firestore();

    //Here, we will save the documents from collection
    var documents = [];

    if (isCollection) {
      //COLLECTIONS - GET DOCUMENTS 

      this.getMeets();

    } else {
      //DOCUMENT - GET FIELDS && COLLECTIONS
      var referenceToCollection = collection.replace("/" + lastSegment, "");

      //Create reference to the document itseld
      //alert(referenceToCollection + "///" + lastSegment);

      var docRef = db.collection(referenceToCollection).doc(lastSegment);

      //Get the starting collectoin
      var parrentCollection = segments;
      parrentCollection.splice(-1, 1);

      //Find the collections of this document
      this.findDocumentCollections(parrentCollection);

      docRef.get().then(function (doc) {
        if (doc.exists) {
          console.log("Document data:", doc.data());
          //Directly process the data
          _this.processRecords(doc.data())
        } else {
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    }
  }

  /**
   * Step 3
   * findDocumentCollections - what collections should we display / currently there is no way to get collection form docuemnt
   * @param {Array} chunks - the collection / documents
   */
  findDocumentCollections(chunks) {
    console.log("Search for the schema now of " + chunks);

    //At start is the complete schema
    var theInsertSchemaObject = INSERT_STRUCTURE;
    var cuurrentFields = null;
    console.log("CHUNKS");
    console.log(chunks);

    //Foreach chunks, find the collections / fields
    chunks.map((item, index) => {
      console.log("current chunk:" + item);

      //Also make the last object any
      //In the process, check if we have each element in our schema
      if (theInsertSchemaObject != null && theInsertSchemaObject && theInsertSchemaObject[item] && theInsertSchemaObject[item]['collections']) {
        var isLastObject = (index == (chunks.length - 1));

        if (isLastObject && theInsertSchemaObject != null && theInsertSchemaObject[item] && theInsertSchemaObject[item]['fields']) {
          cuurrentFields = theInsertSchemaObject[item]['fields'];
        }

        if (isLastObject && theInsertSchemaObject != null) {
          //It is last
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        } else {
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        }
      } else {
        theInsertSchemaObject = [];
      }
      console.log("Current schema");
      console.log(theInsertSchemaObject);


    })

    //Save the collection to be shown as button and fieldsOfOnsert that will be used on inserting object
    this.setState({ collections: theInsertSchemaObject, fieldsOfOnsert: cuurrentFields })
  }

  /**
   * Step 4
   * Processes received records from firebase
   * @param {Object} records
   */
  processRecords(records) {
    console.log(records);

    var fields = {};
    var arrays = {};
    var elements = [];
    var elementsInArray = [];
    var newState = {};
    var directValue = "";
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.elements = elements;
    newState.directValue = directValue;
    newState.elementsInArray = elementsInArray;
    newState.records = null;

    this.setState(newState);

    //Each display is consisted of
    //Fields   - This are string, numbers, photos, dates etc...
    //Arrays   - Arrays of data, ex items:[0:{},1:{},2:{}...]
    //         - Or object with prefixes that match in array
    //Elements - Object that don't match in any prefix for Join - They are represented as buttons.

    //In FireStore
    //GeoPoint
    //DocumentReference

    //If record is of type array , then there is no need for parsing, just directly add the record in the arrays list
    if (Common.getClass(records) == "Array") {
      //Get the last name
      console.log("This is array");
      var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
      var allPathItems = subPath.split("+");
      console.log(allPathItems)
      if (allPathItems.length > 0) {
        var lastItem = allPathItems[allPathItems.length - 1];
        console.log(lastItem);
        arrays[lastItem] = records;

      }
      //this.setState({"arrays":this.state.arrays.push(records)})
    } else if (Common.getClass(records) == "Object") {
      //Parse the Object record
      for (var key in records) {
        if (records.hasOwnProperty(key)) {
          var currentElementClasss = Common.getClass(records[key]);
          console.log(key + "'s class is: " + currentElementClasss);

          //Add the items by their type
          if (currentElementClasss == "Array") {
            //Add it in the arrays  list
            arrays[key] = records[key];
          } else if (currentElementClasss == "Object") {
            //Add it in the elements list
            var isElementMentForTheArray = false; //Do we have to put this object in the array
            for (var i = 0; i < Config.adminConfig.prefixForJoin.length; i++) {
              if (key.indexOf(Config.adminConfig.prefixForJoin[i]) > -1) {
                isElementMentForTheArray = true;
                break;
              }
            }

            var objToInsert = records[key];
            objToInsert.uidOfFirebase = key;

            if (isElementMentForTheArray) {
              //Add this to the merged elements
              elementsInArray.push(objToInsert);
            } else {
              //Add just to elements
              elements.push(objToInsert);
            }

          } else if (currentElementClasss != "undefined" && currentElementClasss != "null") {
            //This is string, number, or Boolean
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "GeoPoint") {
            //This is GeoPOint
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "DocumentReference") {
            //This is DocumentReference
            //Add it to the fields list
            fields[key] = records[key];
          }

        }
      }
    } if (Common.getClass(records) == "String") {
      console.log("We have direct value of string");
      directValue = records;
    }

    //Convert fields from object to array
    var fieldsAsArray = [];
    console.log("Add the items now inside fieldsAsArray");
    console.log("Current schema");
    console.log(this.state.currentInsertStructure)
    //currentInsertStructure
    var keysFromFirebase = Object.keys(fields);
    console.log("keysFromFirebase")
    console.log(keysFromFirebase)
    var keysFromSchema = Object.keys(this.state.currentInsertStructure || {});
    console.log("keysFromSchema")
    console.log(keysFromSchema)

    keysFromSchema.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        var indexOfElementInFirebaseObject = keysFromFirebase.indexOf(key);
        if (indexOfElementInFirebaseObject > -1) {
          keysFromFirebase.splice(indexOfElementInFirebaseObject, 1);
        }
      }
    });

    console.log("keysFromFirebase")
    console.log(keysFromFirebase)

    //pp_add
      var navigation = Config.navigation;
      var itemFound = false;
      var showFields = null;
      for (var i = 0; i < navigation.length && !itemFound; i++) {
        if (navigation[i].editFields && navigation[i].link == "meets") {
          showFields = navigation[i].editFields;
          itemFound = true;
        }
        //Look into the sub menus
        if (navigation[i].subMenus) {
          for (var j = 0; j < navigation[i].subMenus.length; j++) {
            if (navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "meets") {
              if(this.state.completePath.split('+')[8] == "events"){
                showFields = navigation[i].subMenus[j].editFields_events;
                itemFound = true;
              }
              else{
                showFields = navigation[i].subMenus[j].editFields_meets;
                itemFound = true;
              }

            }
          }
        }
      }  


       var pos = -1;
    
        //special processing for document_path
        if (records.document_path && records.document_path != "") {
          this.state.docDir = records.document_path;
        }
        else
          newState.docDir = this.state.club_id + "/" + uuidv1();
        // end pp_add


        keysFromFirebase.forEach((key) => {
          if (fields.hasOwnProperty(key)) {
            if (showFields)
              pos = showFields.indexOf(key);
            if (pos < 0)
              return;
            // fieldsAsArray.push({ "theKey": key, "value": fields[key] })
            // fieldsAsArray.splice(pos, 0, { "theKey": key, "value": fields[key] })
            fieldsAsArray[pos] =  { "theKey": key, "value": fields[key] }
          }
        });

    // keysFromFirebase.forEach((key) => {
    //   if (fields.hasOwnProperty(key)) {
    //     fieldsAsArray.push({ "theKey": key, "value": fields[key] })
    //   }
    // });

    //Get all array names
    var arrayNames = [];
    Object.keys(arrays).forEach((key) => {
      arrayNames.push(key)
    });

    var newState = {};
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.isJustArray = Common.getClass(records) == "Array";
    newState.elements = elements;
    newState.elementsInArray = elementsInArray;
    newState.directValue = directValue;
    newState.records = records;
    newState.isLoading = false;

    console.log("THE elements")
    console.log(elements);

    //Set the new state
    this.setState(newState);

    //Additional init, set the DataTime, check format if something goes wrong
    window.additionalInit();
  }

  /**
   *
   * Create R Update D
   *
   */

  /**
  * processValueToSave  - helper for saving in Firestore , converts value to correct format
  * @param {value} value
  * @param {type} type of field
  */
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }

  /**
  * updatePartOfObject  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {String} firebasePath current firestore path
  * @param {String} byGivvenSubLink force link to field
  * @param {Function} callback function after action
  */
 updatePartOfObject(key, value, dorefresh = false, type = null, firebasePath, byGivvenSubLink = null, callback = null) {
  var subLink = this.state.theSubLink;
  if (byGivvenSubLink != null) {
    subLink = byGivvenSubLink;
  }
  console.log("Sub save " + key + " to " + value + " and the path is " + firebasePath + " and theSubLink is " + subLink);
  var chunks = subLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
  var _this = this;
  //First get the document
  //DOCUMENT - GET FIELDS && COLLECTIONS
  var docRef = firebase.app.firestore().doc(firebasePath);
  docRef.get().then(function (doc) {
    if (doc.exists) {
      var numChunks = chunks.length - 1;
      var doc = doc.data();
      //("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {        
      if (value == "DELETE_VALUE") {
        if (numChunks == 2) {
          if (chunks[1] == "attachments") //special treatment for attachments
          {
  
            var docPath = doc[chunks[1]][chunks[2]];
            docPath = unescape(docPath.slice(1 + docPath.lastIndexOf('/'), docPath.lastIndexOf('?')));
            var dRef = firebase.app.storage().ref().child(docPath);
            // Delete the file
            dRef.delete().then(someval => {
              console.log("Deleted from storage file: ", docPath);
            }).catch(function (error) {
              console.log("Could not delete storage file: " + error);
            });

          }//end special treatment for attachments
          doc[chunks[1]].splice(chunks[2], 1);
        }
        if (numChunks == 1) {
          doc[chunks[1]] = null;
        }
      } else {
        //Normal update, or insert
        if (numChunks == 2) {
          doc[chunks[1]][chunks[2]] = value
        }
        if (numChunks == 1) {
          doc[chunks[1]][key] = value
        }
      }

      console.log("Document data:", doc);
      _this.updateAction(chunks[1], doc[chunks[1]], dorefresh, null, true)
      if (callback) {
        callback();
      }

      //alert(chunks.length-1);
      //_this.processRecords(doc.data())
      //console.log(doc);

    } else {
      console.log("No such document!");
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });

}


  /**
  * Firebase update based on key / value,
  * This function also sets derect name and value
  * @param {String} key
  * @param {String} value
  */

  /**
  * updateAction  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {Boolean} forceObjectSave force saving sub object
  */
  updateAction(key, value, dorefresh = false, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/meets/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {
        if (forceObjectSave) //Maybe coming from a delete action
        {
          var db = firebase.app.firestore();

          //special treatment for attachments. Don't zero out the array
          if (key == "attachments" && !Array.isArray(value))
            value = [];

          var databaseRef = db.doc(this.state.firebasePath);
          var updateObj = {};
          updateObj[key] = value;
          databaseRef.set(updateObj, { merge: true });
        }
        else {
                  //PP: DONT UPDATE THE DB HERE. WAIT FOR THE SAVE
        if (!this.state.editedFields.includes(key))
        this.state.editedFields.push(key);

      if (key == "photo")
        this.state.imageLoading = false;

      this.setState({ [key]: value });

        }



      }

    }
  }

  resetEdits() {
    this.state.editedFields.splice(0, this.state.editedFields.length);
  }

  saveEdits() {
    var firebasePath = (this.props.route.path.replace("/meets/", "").replace(":sub", "")) + (this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    var db = firebase.app.firestore();
    var databaseRef = db.doc(firebasePath);
    var updateObj = {};
    var i;
    for (i = this.state.editedFields.length - 1; i >= 0; --i) {
      updateObj[this.state.editedFields[i]] = this.state[this.state.editedFields[i]];
      this.state.editedFields.splice(i, 1);
    }
    databaseRef.set(updateObj, { merge: true });

  }


  /**
  * addDocumentToCollection  - used recursivly to add collection's document's collections
  * @param {String} name name of the collection
  * @param {FirestoreReference} reference
  */
  addDocumentToCollection(name, reference = null) {

    var pathChunks = this.state.firebasePath.split("/");
    pathChunks.pop();
    var withoutLast = pathChunks.join("/");
    console.log(name + " vs " + withoutLast);
    //Find the fields to be inserted
    var theInsertSchemaObject = INSERT_STRUCTURE[name].fields;
    console.log(JSON.stringify(theInsertSchemaObject));

    //Find the collections to be inserted
    var theInsertSchemaCollections = INSERT_STRUCTURE[name].collections;
    console.log(JSON.stringify(theInsertSchemaCollections));

    //Reference to root firestore or existing document reference
    var db = reference == null ? (pathChunks.length > 1 ? firebase.app.firestore().doc(withoutLast) : firebase.app.firestore()) : reference;

    //Check type of insert
    var isTimestamp = Config.adminConfig.methodOfInsertingNewObjects == "timestamp"

    //Create new element
    var newElementRef = isTimestamp ? db.collection(name).doc(Date.now()) : db.collection(name).doc()

    //Add data to the new element
    //newElementRef.set(theInsertSchemaObject)

    //Go over sub collection and insert them
    for (var i = 0; i < theInsertSchemaCollections.length; i++) {
      this.addDocumentToCollection(theInsertSchemaCollections[i], newElementRef)
    }


    //Show the notification on root element
    if (reference == null) {
      this.cancelAddFirstItem();
      this.setState({ notifications: [{ type: "success", content: "Element added. You can find it in the table below." }] });
      this.refreshDataAndHideNotification();
    }
  }

  /**
  * addKey
  * Adds key in our list of fields in firestore
  */
  addKey() {
    if (this.state.NAME_OF_THE_NEW_KEY && this.state.NAME_OF_THE_NEW_KEY.length > 0) {

      if (this.state.VALUE_OF_THE_NEW_KEY && this.state.VALUE_OF_THE_NEW_KEY.length > 0) {

        this.setState({ notifications: [{ type: "success", content: "New key added." }] });
        this.updateAction(this.state.NAME_OF_THE_NEW_KEY, this.state.VALUE_OF_THE_NEW_KEY);
        this.refs.simpleDialog.hide()
        this.refreshDataAndHideNotification();
      }
    }
  }

  /**
  * addItemToArray  - add item to array
  * @param {String} name name of the array
  * @param {Number} howLongItIs count of items, to know the next index
  */
  addItemToArray(name, howLongItIs) {
    console.log("Add item to array " + name);
    console.log("Is just array " + this.state.isJustArray);

    console.log("Data ");
    console.log(this.state.fieldsOfOnsert);

    var dataToInsert = null;
    var correctPathToInsertIn = "";
    if (this.state.fieldsOfOnsert) {
      if (this.state.isJustArray) {
        console.log("THIS IS Array")
        dataToInsert = this.state.fieldsOfOnsert[0];
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      } else {
        dataToInsert = this.state.fieldsOfOnsert[name];
        dataToInsert = dataToInsert ? dataToInsert[0] : null;
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + name + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      }
    }

    console.log("Data to insert");
    console.log(dataToInsert);
    console.log("Path to insert");
    console.log(correctPathToInsertIn);

    var _this = this;
    this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", dataToInsert, true, null, this.state.firebasePath, correctPathToInsertIn, function (e) {
      _this.setState({ notifications: [{ type: "success", content: "New element added." }] });
      _this.refreshDataAndHideNotification();
    })
  }

  /**
  *
  * C Read U D
  *
  */

  /**
  * showSubItems - displays sub object, mimics opening of new page
  * @param {String} theSubLink , direct link to the sub object
  */
  showSubItems(theSubLink) {
    var chunks = theSubLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    this.setState({
      itemOfInterest: chunks[1],
      theSubLink: theSubLink,
    })
    var items = this.state.records;
    for (var i = 1; i < chunks.length; i++) {
      console.log(chunks[i]);
      items = items[chunks[i]];
    }
    console.log("--- NEW ITEMS ");
    console.log(items)
    this.processRecords(items);
  }

  /**
  *
  * C R U Delete
  *
  */

  /**
  * deleteFieldAction - displays sub object, mimics opening of new page
  * @param {String} key to be updated
  * @param {Boolean} isItArrayItem 
  * @param {String} theLink 
  */
 deleteFieldAction(key, isItArrayItem = false, theLink = null) {
  console.log("Delete " + key);
  console.log(theLink);
  if (theLink != null) {
    theLink = theLink.replace("/meets", "");
  }
  if (isNaN(key)) {
    isItArrayItem = false;
  }

  //special treatment when deleting attachments
  if (isItArrayItem)
    this.state.deleteAlertTitle = "This attachment will be deleted permanently";
  else
    this.state.deleteAlertTitle = "All data associated with this " + this.state.itemToDelete + " will be deleted!";

  console.log("Is it array: " + isItArrayItem);
  var firebasePathToDelete = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
  if (key != null) {
    //firebasePathToDelete+=("/"+key)
  }

  console.log("firebasePath for delete:" + firebasePathToDelete);
  this.setState({ deleteAlert: true, pathToDelete: theLink ? theLink : firebasePathToDelete, isItArrayItemToDelete: isItArrayItem, keyToDelete: theLink ? "" : key });

}

  /**
  * doDelete - do the actual deleting based on the data in the state
  */
 doDelete() {
    var _this = this;

    var completeDeletePath = this.state.pathToDelete + "/" + this.state.keyToDelete;

    if (this.state.pathToDelete.indexOf(Config.adminConfig.urlSeparatorFirestoreSubArray) > -1) {
      //Sub data
      // _this.refs.deleteDialog.hide();
      this.hideAlert("deleteAlert");
      this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {
        _this.setState({ notifications: [{ type: "success", content: "Element deleted." }] });
        _this.refreshDataAndHideNotification();
      })
    } else {
      //Normal data

      var chunks = completeDeletePath.split("/");

      var db = firebase.app.firestore();


      if (chunks.length % 2) {
        //odd
        //Delete fields from docuemnt
        var refToDoc = db.doc(this.state.pathToDelete);

        // Remove the 'capital' field from the document
        var deleteAction = {};
        deleteAction[this.state.keyToDelete] = firebaseREF.firestore.FieldValue.delete();
        var removeKey = refToDoc.update(deleteAction).then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, keyToDelete: null, pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });
      } else {
        //even
        //delete document from collection
        //alert("Delete document "+completeDeletePath);
        //db.collection(this.state.pathToDelete).doc(this.state.keyToDelete).delete().then(function() {
        db.collection(this.state.firebasePath).doc(this.state.keyToDelete).delete().then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, pathToDelete: null, notifications: [{ type: "success", content: "Athlete deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing athlete: ", error);
        });

      }

    }




    /*firebase.database().ref(this.state.pathToDelete).set(null).then((e)=>{
      console.log("Delete res: "+e)
      this.refs.deleteDialog.hide();
      this.setState({keyToDelete:null,pathToDelete:null,notifications:[{type:"success",content:"Field is deleted."}]});
      this.refreshDataAndHideNotification();

    })*/
  }



  // TO  Delete a Meet
  deleteMeet(evt) {
    evt.stopPropagation();

    this.state.itemToDelete = "meet"
    this.deleteFieldAction(evt.target.id);
  }
  createMeetEditButton(id){
    var theLink = "meets/aquaticsID+India+associations+KSA+meets+"+ id;
    return (<Link to={theLink}>
              <button id={id} onClick={this.editMeet}
                className="btn btn-rose add-event-session-btn">Edit Meet
              </button>
  </Link>)
  }
  //To Edit a Meet
  editMeet(evt){
    evt.stopPropagation();
    this.getCollectionDataFromFireStore("aquaticsID/India/associations/KSA/meets/"+ evt.target.id);   
    this.props.params.sub = "aquaticsID/India/associations/KSA/meets/"+ evt.target.id;
  }
  //to close a Meet
  closeMeet(evt){
    evt.stopPropagation();
    this.showAlert("closeMeetAlert");
  }
  confirmCloseMeet(){
    alert("Closed");
    this.hideAlert("closeMeetAlert");
  }
  cancelCloseMeet(){
    this.hideAlert("closeMeetAlert");
  }

  // TO  Delete a session
  deleteSession(evt) {
    evt.stopPropagation()
    this.state.itemToDelete = "session"
    this.state.deleteMeetId = evt.target.id.split("_")[0];
    this.state.deleteSessionId = evt.target.id.split("_")[1];

    var id = this.state.deleteMeetId + "/sessions/" + this.state.deleteSessionId
    this.deleteFieldAction(id);
  }



  /**
  * cancelDelete - user click on cancel
  */
  cancelDelete() {
    console.log("Cancel Delete");
    this.refs.deleteDialog.hide()
  }

  cancelAddFirstItem() {
    console.log("Cancel Add");
    this.refs.addCollectionDialog.hide()
  }

  cancelAddNewMeet() {
    var newState = {};

    newState.meet_name = "";
    newState.venue = "", 
    newState.from = "";
    newState.to = "";
    newState.course = "Short(25m)";
    newState.other_course = "";  
    newState.max_events = "";
    newState.description = "";
    newState.links = [];

    newState.country = "India";
    this.setState(newState);

    this.refs.addnewitemDialog.hide();
  }
  cancelAddNewSession() {
    var newState = {};

    newState.session_name = "";
    this.setState(newState);

    this.refs.addNewSessionDialog.hide();
  }

  cancelAddNewEvent() {
    var newState = {};

    newState.distance = "100m";
    newState.stroke = "Free Style";
    newState.category = "Boys/Men";
    newState.swim_group = "I";
    newState.event_number = "";

    this.setState(newState);
    // alert("Cancel")
    this.refs.addNewEventDialog.hide();
  }


  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    if (name == "max_events") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }
    if(name == "event_number"){
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }
    
    this.setState({
      [name]: value
    });
  }

  validateInputs() {
    if (this.state.mobile_number.length < 10 || this.state.pin_code.length < 6)
      return false;

    return true;

  }

  markUploaderStart() {
    this.state.imageLoading = true;
  }

  refreshEvents(meetId, sessionId, notify = true) {
    var db = firebase.app.firestore();
    var top_path = "aquaticsID/India/associations/KSA/meets";
    var _this = this;

    if (!notify) {
      for (var idx = this.state.events.length - 1; idx >= 0; idx--) {
        var ev = this.state.events[idx].data;
        if (ev.meet_id == meetId && ev.session_id == sessionId)
          this.state.events.splice(idx, 1);

      }
    }

    var levents = this.state.events.slice();

    db.collection(top_path + "/" + meetId + "/sessions/" + sessionId + "/events").orderBy("sequence").get()
      .then(function (event_snap) {
        event_snap.forEach(function (event_doc) {
          var edoc = event_doc.data();
          edoc.event_id = event_doc.id;
          edoc.session_id = sessionId;
          edoc.meet_id = meetId;
          edoc.uidOfFirebase = edoc.meet_id + "+sessions+" + edoc.session_id + "+events+" + edoc.event_id;

          var event_exists = false;
          for (event of levents)
            if (event.id == event_doc.id)
              event_exists = true;
          if (!event_exists) {
            levents.push({ id: event_doc.id, data: edoc });
          }
        });
        //set state
        var shout_out = notify ? [{ type: "success", content: "New Event created. You can find it in the table below." }] : [];
        _this.setState({ events: levents, isCollection: true, debounce: false, notifications: shout_out, swapLoading: false })
      });
  }

  addNewMeet(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */

    var db = firebase.app.firestore();

    if(this.state.venue == ""){
      this.state.errorStatement = "Please ensure you have selected venue for the meet!"
      this.showAlert("dateAlert");
      return false;
    }
    if (this.state.from === "" || this.state.to === "") {
      this.state.errorStatement = "Please ensure you have selected dates for the meet!"
      this.showAlert("dateAlert");
      return false;
    }

    if(this.state.isUploading){
      this.showAlert("loadingAlert");
      return false;
    }

    if(this.state.course == "Other"){
      this.state.course = this.state.other_course;
    }

    if(Date.parse(this.state.from) > Date.parse(this.state.to) ){
      this.state.errorStatement = "TO date should not be less than FROM date!"
      this.showAlert("dateAlert");
      return false;
     }


    if (!this.state.debounce) {
      this.state.debounce = true;
      db.collection("aquaticsID/India/associations/KSA/meets").add({
        meet_name: this.state.meet_name,
        venue: this.state.venue,
        from: this.state.from,
        to: this.state.to,
        course: this.state.course,
        max_events: this.state.max_events,
        description: this.state.description,
        attachments: this.state.links
      })
        .then(docRef => {
          this.refs.addnewitemDialog.hide()
          this.refs.addCollectionDialog.hide()
          this.refreshDataAndHideNotification()
          this.setState({ debounce: false, notifications: [{ type: "success", content: "New Meet created. You can find it in the table below." }] })
        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });
    }
  }

  addNewSession(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */

    var db = firebase.app.firestore();

    if (!this.state.debounce) {
      this.state.debounce = true;
      db.collection("aquaticsID/India/associations/KSA/meets/" + this.state.changeMeetId + "/sessions").add({
        session_name: this.state.session_name,
      })
        .then(docRef => {
          this.refs.addNewSessionDialog.hide()
          this.refreshDataAndHideNotification()
          this.setState({ debounce: false, notifications: [{ type: "success", content: "New Session created. You can find it in the table below." }] })
        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });
    }

  }


  addNewEvent(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */


    var db = firebase.app.firestore();

    // alert(this.state.changeMeetId )
   
    for(var event of this.state.events){
      if(event.data.meet_id == this.state.changeMeetId){
        if(event.data.distance == this.state.distance &&
            event.data.stroke == this.state.stroke &&
            event.data.category == this.state.category &&
            event.data.swim_group == this.state.swim_group){
              this.state.errorStatement = "Please ensure you have selected different Event!"
              this.showAlert("dateAlert");
              return false;
          }
        if(event.data.event_number == this.state.event_number){
          this.state.errorStatement = "Please ensure you have entered unique event number!"
          this.showAlert("dateAlert");
          return false;
        }
      }      
    }

    if (!this.state.debounce) {
      this.state.debounce = true;
      db.collection("aquaticsID/India/associations/KSA/meets/" + this.state.changeMeetId + "/sessions/" + this.state.changeSessionId + "/events").add({
        distance: this.state.distance,
        stroke: this.state.stroke,
        category: this.state.category,
        swim_group: this.state.swim_group,
        event_number: this.state.event_number,
        sequence: this.state.events.length + 1
      })
        .then(docRef => {
          this.refs.addNewEventDialog.hide()
          this.refreshEvents(this.state.changeMeetId, this.state.changeSessionId);
          this.refreshDataAndHideNotification(false);
            var newState = {};
              newState.event_number = "";
            this.setState(newState);

        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });
    }

  }
    //Attachments
    updateAttachments(e) {
      e.preventDefault(); // <- prevent form submit from reloading the page
  
      /* Send the message to Firebase */
      var firebasePath = (this.props.route.path.replace("/meets/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
      var db = firebase.app.firestore();
      if (this.state.isUploading) {
        this.showAlert("loadingAlert");
        return;
      }
      //var databaseRef = db.doc(firebasePath);
      var databaseRef = db.doc(this.state.firebasePath);
  
      var acopy = Object.assign({}, this.state.arrays);
  
      // var new_attachments = this.state.arrays["attachments"].concat(this.state.links);
      acopy.attachments = acopy.attachments.slice().concat(this.state.links);
  
      databaseRef.update({ attachments: acopy.attachments })
        .then(someval => {
          console.log("Updated athlete attachments");
  
          this.setState({ arrays: acopy, links: [] });
  
          this.refs.addattachments.hide();
        })
        .catch(err => {
          alert("Error updating athlete attachments: ", err);
        })
  
    }
    cancelAttachments() {
      var newState = {}
      newState.links = this.state.links.slice();
  
      newState.links.map(function (docPath) {
        docPath = unescape(docPath.slice(1 + docPath.lastIndexOf('/'), docPath.lastIndexOf('?')));
        var dRef = firebase.app.storage().ref().child(docPath);
  
        // Delete the file
        dRef.delete().then(someval => {
          console.log("Deleted from storage file: ", docPath);
        }).catch(function (error) {
          console.log("Could not delete storage file: " + error);
        });
      })
  
      newState.links = [];
      this.setState(newState);
  
      this.refs.addattachments.hide();
    }
  


  /**
  *
  * UI GENERATORS
  *
  */

  /**
  * This function finds the headers for the current menu
  * @param firebasePath - we will use current firebasePath to find the current menu
  */
  findHeadersBasedOnPath(firebasePath) {
    var headers = null;

    var itemFound = false;
    var navigation = Config.navigation;
    var showFields = null;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].tableFields && navigation[i].link == "meets") {
        headers = navigation[i].tableFields;
        itemFound = true;
      }

      //Look into the sub menus
      if (navigation[i].subMenus) {
        for (var j = 0; j < navigation[i].subMenus.length; j++) {
          if (navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "meets") {
            headers = navigation[i].subMenus[j].tableFields;
            itemFound = true;
          }
        }
      }
    }
    return headers;
  }

  addSessionBtnHandler(evt) {
    this.state.changeMeetId = evt.target.id;
    this.refs.addNewSessionDialog.show();
    evt.stopPropagation()
  }


  addEventBtnHandler(evt) {
    this.state.changeMeetId = evt.target.id.split("_")[0];
    this.state.changeSessionId = evt.target.id.split("_")[1];
    this.refs.addNewEventDialog.show();
    evt.stopPropagation()
  }

  listMeets() {
    var name = this.state.currentCollectionName;
    // var name = "Meets";
    var component = [];


    for (var meet of this.state.meets) { //------> For each meet
      //****** Set the meet title *****
      var meet_title =
        <AccordionItemTitle>
          <div className="row">
            <div className="col-sm-8">
              <h3 className="u-position-relative">
                { meet.data.meet_name}
              </h3>
            </div>
            <div className="col-sm-4">
              <button id={meet.id} onClick={this.deleteMeet}
                className="btn btn-rose delete-event-session-btn">Delete Meet
              </button>
              {/* <button id={meet.id} onClick={this.editMeet}
                className="btn btn-rose add-event-session-btn">Edit Meet
              </button> */}
              {this.createMeetEditButton(meet.id)}
              <button id={meet.id} onClick={this.closeMeet}
                className="btn btn-rose close-event-session-btn">Close Meet
              </button>

              <div className="accordion__arrow" role="presentation" />
            </div>
          </div>
          <p>From: {meet.data.from.toLocaleString()}, TO: {meet.data.to.toLocaleString()}, Location: {meet.data.venue}</p>
        </AccordionItemTitle>;

      var session_bodies = [];

      for (var session of this.state.sessions) { //--------> look through all sessions

        if (session.data.meet_id == meet.id) { //---------> if session belongs to this meet

          var event_docs = [];
          var event_bodies = [];

          for (var event of this.state.events) //------> look through all events  
            if (event.data.session_id == session.id) //------> if event belongs to this session
              event_docs.push(event.data);

          event_bodies.push(
            <SimpleTable
              headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
              deleteFieldAction={this.deleteFieldAction}
              fromObjectInArray={true}
              name={name}
              routerPath={this.props.route.path}
              isJustArray={false}
              sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
              data={event_docs}
              swapSequence={this.swapSequence}
            />
          );
          //**** Set the session title ****
          var session_title =
          <div className="row">
            <div className="col-sm-9">
              <h5 className="u-position-relative">
                {session.data.session_name}  ({event_docs.length})
              </h5>
            </div>
            <div className="col-sm-3">
              <button id={meet.id + "_" + session.id} onClick={this.deleteSession}
                className="btn btn-rose delete-event-session-btn">Delete Session</button>

              <button id={meet.id + "_" + session.id} onClick={this.addEventBtnHandler}
                className="btn btn-rose add-event-session-btn">Add New Event</button>


              <div className="accordion__arrow margin-right-2" role="presentation" />
            </div>
          </div>

          //------- Each session is an accordion in itself --------
          session_bodies.push(
            <Accordion>
              <AccordionItem>
                <AccordionItemTitle>{session_title}</AccordionItemTitle>
                <AccordionItemBody>{event_bodies}</AccordionItemBody>
              </AccordionItem>
            </Accordion>
          );

        }

      }


      //Now push the entire meet into the component
      component.push(
        <Accordion>
          <AccordionItem>
            <AccordionItemTitle>{meet_title}</AccordionItemTitle>
            <AccordionItemBody>
              <h4>Sessions
                <button id={meet.id} onClick={this.addSessionBtnHandler}
                 className="btn btn-rose add-session-btn">Add New Session
                </button>
              </h4>
              {session_bodies}
            </AccordionItemBody>
          </AccordionItem>
        </Accordion>
      );

    }

    return (<div className="col-md-12" key={name}>
      <div className="card">
        <div className="card-header card-header-icon" data-background-color="rose">
          <i className="material-icons">assignment</i>
        </div>
        <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addnewitemDialog.show(); }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
          <i className="material-icons">add</i>
        </div></a>
        <div className="card-content">
          <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
          <div className="toolbar">
          </div>
          <div className="material-datatables">
            {component}
          </div>
        </div>
      </div>
    </div>);

  } //----- end listMeets -----


  /**
  * makeCollectionTable
  * Creates single collection documents
  */
  makeCollectionTable() {
    var name = this.state.currentCollectionName;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          {/*begin pp_add*/}
          <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addnewitemDialog.show(); }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            {/*end pp_add*/}
            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={true}
                name={name}
                routerPath={this.props.route.path}
                isJustArray={false}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.documents}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  // /**
  //  * Creates single array section
  //  * @param {String} name, used as key also
  //  */
  // makeArrayCard(name) {
  //   return (
  //     <div className="col-md-12" key={name}>
  //       <div className="card">
  //         <div className="card-header card-header-icon" data-background-color="rose">
  //           <i className="material-icons">assignment</i>
  //         </div>
  //         <a onClick={() => { this.addItemToArray(name, this.state.arrays[name].length) }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
  //           <i className="material-icons">add</i>
  //         </div></a>
  //         <div className="card-content">
  //           <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
  //           <div className="toolbar">

  //           </div>
  //           <div className="material-datatables">
  //             <Table
  //               isFirestoreSubArray={true}
  //               showSubItems={this.showSubItems}
  //               headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
  //               deleteFieldAction={this.deleteFieldAction}
  //               fromObjectInArray={false} name={name}
  //               routerPath={this.props.route.path}
  //               isJustArray={this.state.isJustArray}
  //               sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
  //               data={this.state.arrays[name]} />
  //           </div>
  //         </div>
  //       </div>
  //     </div>
  //   )
  // }

  /**
   * Creates  table section for the elements object
   * @param {String} name, used as key also
   */
  makeTableCardForElementsInArray() {
    var name = this.state.lastPathItem;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)} deleteFieldAction={this.deleteFieldAction} fromObjectInArray={true} name={name} routerPath={this.props.route.path} isJustArray={this.state.isJustArray} sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""} data={this.state.elementsInArray}>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  //generate docs table in edit page
  makeArrayCard(name) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addattachments.show(); }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <MeetDocsTable
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                //headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                headers={["name"]}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={false} name={name}
                routerPath={this.props.route.path}
                isJustArray={this.state.isJustArray}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.arrays[name]} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
    * Creates direct value section
    * @param {String} value, valu of the current path
    */
  makeValueCard(value) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <div className="card-content">
            <h4 className="card-title">Value</h4>
            <div className="toolbar">
            </div>
            <div>
              <Input updateAction={this.updateAction} className="" theKey="DIRECT_VALUE_OF_CURRENT_PATH" value={value} />
            </div>
          </div>
        </div>
      </div>
    )
  }


  /**
   * generateBreadCrumb
   */
  generateBreadCrumb() {
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : "";
    var items = subPath.split(Config.adminConfig.urlSeparator);
    var path = "/assoc/";
    return (<div>{items.map((item, index) => {
      if (index == 0) {
        path += item;
      } else {
        path += "+" + item;
      }

      return (<Link className="navbar-brand" to={path}>{item} <span className="breadcrumbSeparator">{index == items.length - 1 ? "" : "/"}</span><div className="ripple-container"></div></Link>)
    })}</div>)
  }

  /**
   * generateNotifications
   * @param {Object} item - notification to be created
   */
  generateNotifications(item) {
    return (
      <div className="col-md-12">
        <Notification type={item.type} >{item.content}</Notification>
      </div>
    )
  }

  /**
  * refreshDataAndHideNotification
  * @param {Boolean} refreshData 
  * @param {Number} time 
  */
  refreshDataAndHideNotification(refreshData = true, time = 3000) {
    //Refresh data,
    if (refreshData) {
      this.resetDataFunction();
    }

    //Hide notifications
    setTimeout(function () { this.setState({ notifications: [] }) }.bind(this), time);
  }

  //MAIN RENDER FUNCTION 
  render() {
    //begin pp_add
    var addnewitemDialog = {
      width: '50%',
      height: 'auto',
      marginLeft: '-25%',
      // marginTop:'-20%',
      position: 'absolute',
      padding: '0px'
    };
    //styling for add attachments skylight
    var addattactments = {
      width: '40%',
      height: 'auto',
      position: 'absolute',
      marginLeft: '-20%',
      padding: '0px'
    };
    //end pp_add
    return (
      <div className="content">
        {/*<NavBar>{this.generateBreadCrumb()}</NavBar>*/}
        <NavBar>
          <div className="pull-right">
            <a href="http://karnatakaswimming.org" target="_blank"> <img className="img-circle" src='assets/img/ksa_logo.png' height="70" width="80" /></a>
          </div>
        </NavBar>


        <div className="content" sub={this.state.lastSub}>

          <div className="container-fluid">

            <div style={{ textAlign: 'center' }}>
              {/* LOADER */}
              {this.state.isLoading ? <Loader color="#8637AD" size="12px" margin="4px" /> : ""}
            </div>

            {/* NOTIFICATIONS */}
            {this.state.notifications ? this.state.notifications.map((notification) => {
              return this.generateNotifications(notification)
            }) : ""}

            {/* SWEET ALERTS */}
            <SweetAlert
              warning
              show={this.state.dateAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ dateAlert: false })}>
              {this.state.errorStatement}

            </SweetAlert>

            {/* SWEET ALERTS */}
            <SweetAlert
              show={this.state.swapLoading}
              title=" Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
            </SweetAlert>

            {/* sweet photo loading alert */}
            <SweetAlert
              show={this.state.loadingAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ loadingAlert: false })}>
              Please wait! Files are uploading...
            </SweetAlert>


            {/* sweet delete alert */}
            <SweetAlert
              warning
              showCancel
              show={this.state.deleteAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="danger"
              cancelBtnCssClass="deleteAlertBtnColor"
              title="Are you sure?"
              onConfirm={() => this.doDelete()}
              onCancel={() => this.cancelAlertDelete()}
            >
                 {this.state.deleteAlertTitle}
            </SweetAlert>

            {/* sweet delete alert */}
            {/* <SweetAlert
              warning
              showCancel
              show={this.state.deleteAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="default"
              title="Are you sure?"
              onConfirm={() => this.doDelete()}
              onCancel={() => this.setState({ deleteAlert: false })}
            >
              {this.state.deleteAlertTitle}
            </SweetAlert> */}

              {/* sweet close alert */}
              <SweetAlert
              warning
              showCancel
              show={this.state.closeMeetAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="danger"
              cancelBtnCssClass="deleteAlertBtnColor"
              title="Are you sure?"
              onConfirm={() => this.confirmCloseMeet()}
              onCancel={() => this.cancelCloseMeet()}
            >
              You want to stop registrations for this meet?
            </SweetAlert>

            {/* Documents in collection */}
            {this.state.isCollection && this.state.meets.length > 0 ? this.listMeets() : ""}

            {/* DIRECT VALUE */}
            {this.state.directValue && this.state.directValue.length > 0 ? this.makeValueCard(this.state.directValue) : ""}

            {/* ARRAYS */}
            {this.state.arrayNames ? this.state.arrayNames.map((key) => {
              return this.makeArrayCard(key)
            }) : ""}

            {/* FIELDS */}
            {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? (<div className="col-md-12">
              <div className="card">
                {/*
                <a  onClick={()=>{this.refs.simpleDialog.show()}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}>
                    <i className="material-icons">add</i>
                </div></a>*/}
                <form className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{Common.capitalizeFirstLetter(Config.adminConfig.fieldBoxName)}</h4>
                  </div>
                  {this.state.fieldsAsArray ? this.state.fieldsAsArray.map((item) => {

                    return (
                      <Fields
                        isFirestore={true}
                        parentKey={null}
                        key={item.theKey + this.state.lastSub}
                        deleteFieldAction={this.deleteFieldAction}
                        updateAction={this.updateAction}
                        theKey={item.theKey}
                        value={item.value} />)


                  }) : ""}
                  <div
                    className="text-center">

                    <Link to='meets/aquaticsID+India+associations+KSA+meets'>
                      <button onClick={this.saveEdits}
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Save">Save</button>
                    </Link>

                    <Link to='meets/aquaticsID+India+associations+KSA+meets'>
                      <button onClick={this.resetEdits}
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Cancel">Cancel</button>
                    </Link>

                  </div>

                </form>
              </div>
            </div>) : ""}


            {/* COLLECTIONS */}
            {this.state.theSubLink == null && this.state.isDocument && this.state.collections && this.state.collections.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{"Collections"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.theSubLink == null && this.state.collections ? this.state.collections.map((item) => {
                      var theLink = "/assoc/" + this.state.completePath + Config.adminConfig.urlSeparator + item;
                      return (<Link to={theLink}><a className="btn">{item}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}

       

            {/* ELEMENTS MERGED IN ARRAY */}
            {this.state.elementsInArray && this.state.elementsInArray.length > 0 ? (this.makeTableCardForElementsInArray()) : ""}

            {/* ELEMENTS */}
            {this.state.elements && this.state.elements.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{this.state.lastPathItem + "' elements"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.elements ? this.state.elements.map((item) => {
                      var theLink = "/fireadmin/" + this.state.completePath + Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
                      return (<Link onClick={() => { this.showSubItems(theLink) }}><a className="btn">{item.uidOfFirebase}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}




          </div>
        </div>

        <SkyLight hideOnOverlayClicked ref="addCollectionDialog" title="">
          <span><h3 className="center-block">Add first document in collection</h3></span>
          <div className="col-md-12">
            <Notification type="success" >Looks like there are no documents in this collection. Add your first document in this collection</Notification>
          </div>

          {/* <div className="col-md-12">
            Data Location
          </div>
          <div className="col-md-12">
            <b>{this.state.showAddCollection}</b>
          </div> */}


          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelAddFirstItem} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.openAddNewMeet} className="btn btn-success center-block">ADD</a>
            </div>

          </div>

        </SkyLight>

        {/* Add New Meet */}
        {/*begin pp_add*/}
        <SkyLight dialogStyles={addnewitemDialog} ref="addnewitemDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Add New Meet</h4>

              <form className="padding-10" onSubmit={this.addNewMeet}>
                <div className="row">
                  <div className="col-sm-1"></div>
                  <div className="col-sm-10">
                    {/* Meet Name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Meet Name :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="meet_name" aria-required="true" value={this.state.meet_name} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Meet Name */}
                    {/* address */}
                    {/* <div className="row margin-b-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >Location :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <textarea required="true" name="address" value={this.state.address} onChange={this.handleInputChange} className="form-control" rows="4" cols="21" />
                          </div>
                        </div>
                      </div>
                    </div> */}
                    {/* address */}
                    {/* venue */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >Venue :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <SelectVenue theKey="venue" updateAction={this.updateAction} value={this.state.venue} options={this.state.meetVenues} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* venue */}
                    {/* city */}
                    {/* <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >City :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="city" aria-required="true" value={this.state.city} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div> */}
                    {/* city */}
                    {/* State */}
                    {/* <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >State :</label>
                      </div>
                      <div className="col-sm-7 margin-padding-0" style={{ paddingRight: "6%" }} >
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <select className="select_width" name="state" aria-required="true" value={this.state.state} onChange={this.handleInputChange}>
                              <option value="Andhra Pradesh">Andhra Pradesh</option>
                              <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                              <option value="Assam">Assam</option>
                              <option value="Bihar">Bihar</option>
                              <option value="Chhattisgarh">Chhattisgarh</option>
                              <option value="Goa">Goa</option>
                              <option value="Gujarat">Gujarat</option>
                              <option value="Haryana">Haryana</option>
                              <option value="Himachal Pradesh">Himachal Pradesh</option>
                              <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                              <option value="Jharkhand">Jharkhand</option>
                              <option value="Karnataka">Karnataka</option>
                              <option value="Kerala">Kerala</option>
                              <option value="Madhya Pradesh">Madhya Pradesh</option>
                              <option value="Maharashtra">Maharashtra</option>
                              <option value="Manipur">Manipur</option>
                              <option value="Meghalaya">Meghalaya</option>
                              <option value="Mizoram">Mizoram</option>
                              <option value="Nagaland">Nagaland</option>
                              <option value="Odisha">Odisha</option>
                              <option value="Punjab">Punjab</option>
                              <option value="Rajasthan">Rajasthan</option>
                              <option value="Sikkim">Sikkim</option>
                              <option value="Tamil Nadu">Tamil Nadu</option>
                              <option value="Telangana">Telangana</option>
                              <option value="Tripura">Tripura</option>
                              <option value="Uttar Pradesh">Uttar Pradesh</option>
                              <option value="Uttarakhand">Uttarakhand</option>
                              <option value="West Bengal">West Bengal</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div> */}
                    {/* State */}
                    {/* pincode */}
                    {/* <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >Pin Code :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="pin_code" aria-required="true" value={this.state.pin_code} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div> */}
                    {/* pincode */}
                    {/* From and To */}
                    {/* From */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">From :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <DateTime required="true" inputProps={{ placeholder: 'DD/MM/YYYY HH:MM', readOnly: false }} theKey="from" value={this.state.from} updateAction={this.updateAction} dateFormats="DD-MM-YYYY" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* From */}
                    {/* To */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">To :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <DateTime required="true" inputProps={{ placeholder: 'DD/MM/YYYY HH:MM', readOnly: false }} validator={this.validDOB} theKey="to" value={this.state.to} updateAction={this.updateAction} dateFormats="DD-MM-YYYY" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* TO */}
                    {/* From and To */}
                    {/* Course */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Course :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <Select theKey="course" updateAction={this.updateAction} value={this.state.course} options={["Short (25m)", "Long (50m)", "Open Water", "Other"]} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Course */}
                    {/*Manual Entry of Course */}
                    {this.state.course == "Other" && <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Course Name :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="other_course" aria-required="true" value={this.state.other_course} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>}
                    {/* Manual Entry of Course */}
                    {/* Max Events Per Athlete */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >Max Events Per Athlete :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="max_events" aria-required="true" value={this.state.max_events} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div> 
                    {/* Max Events Per Athlete */}
                    {/* Description */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >Description :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <TextArea theKey="description" updateAction={this.updateAction} value={this.state.description}/>
                          </div>
                        </div>
                      </div>
                    </div> 
                    {/* Description */}
                    {/* Documents */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right padding-right-5">
                        <label className="control-label label-color" >Documents :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <ol className="ol-styling">
                              {this.displayList()}
                            </ol>
                            <label style={{ backgroundColor: '#E82062', color: 'white', padding: 10, borderRadius: 4, pointer: 'cursor', marginLeft: '40%', marginTop: '5px' }}>
                              Select Files
                                <FileUploader
                                hidden
                                accept=".gif, .jpg, .png, .pdf, .doc, .docx"
                                name="attachments"
                                multiple
                                storageRef={firebase.app.storage().ref('meet_docs')}
                                onUploadStart={this.handleUploadStart}
                                onUploadError={this.handleUploadError}
                                onUploadSuccess={this.handleUploadSuccess}
                                onProgress={this.handleProgress}
                              />
                            </label>
                            <Indicator show={this.state.isUploading} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Documents */}
                  </div>

                  <div className="col-sm-1"></div>
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                      <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                      <button onClick={this.cancelAddNewMeet} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/*end pp_add*/}
        {/* End Add new Meet */}

        {/* Add New Session */}
        <SkyLight dialogStyles={addnewitemDialog} ref="addNewSessionDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Add New Session</h4>

              <form className="padding-10" onSubmit={this.addNewSession}>
                <div className="row">
                  <div className="col-sm-1"></div>
                  <div className="col-sm-10">
                    {/* Session Name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Session Name :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="session_name" aria-required="true" value={this.state.session_name} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Session Name */}
                  </div>
                  <div className="col-sm-1"></div>
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                      <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                      <button onClick={this.cancelAddNewSession} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/* End Add new Session */}

        {/* Add New Event */}
        <SkyLight dialogStyles={addnewitemDialog} ref="addNewEventDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Add New Event</h4>

              <form className="padding-10" onSubmit={this.addNewEvent}>
                <div className="row">
                  <div className="col-sm-1"></div>
                  <div className="col-sm-10">
                    {/* Distance */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Distance :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <Select theKey="distance" updateAction={this.updateAction} value={this.state.distance} options={["25m", "50m", "100m", "200m", "400m", "800m", "1500m"]} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Distance */}
                    {/* Stroke */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Stroke :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <Select theKey="stroke" updateAction={this.updateAction} value={this.state.stroke} options={["Free Style", "Butterfly", "Breast Stroke", "Back Stroke", "Individual Medley"]} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* stroke */}
                    {/* Category */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Category :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <Select theKey="category" updateAction={this.updateAction} value={this.state.category} options={["Boys/Men", "Girls/Women"]} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Category */}
                    {/* Group */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color">Group :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <Select theKey="swim_group" updateAction={this.updateAction} value={this.state.swim_group} options={["I", "II", "III", "IV", "V", "Open"]} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Group */}
                    {/* order number */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-3 text-align-right">
                        <label className="control-label label-color" >Event Order Number :</label>
                      </div>
                      <div className="col-sm-9 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="event_number" aria-required="true" value={this.state.event_number} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* order number */}
                  </div>
                  <div className="col-sm-1"></div>
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                      <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                      <button onClick={this.cancelAddNewEvent} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/* End Add new Event */}

        <SkyLight hideOnOverlayClicked ref="simpleDialog" title="">
          <span><h3 className="center-block">Add new key</h3></span>
          <br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Name of they key</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="NAME_OF_THE_NEW_KEY" value={"name"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div><br /><br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Value</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="VALUE_OF_THE_NEW_KEY" value={"value"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div>
          <div className="col-sm-12 ">
            <div className="col-sm-3 ">
            </div>
            <div className="col-sm-6 center-block">
              <a onClick={this.addKey} className="btn btn-rose btn-round center-block"><i className="fa fa-save"></i>   Add key</a>
            </div>
            <div className="col-sm-3 ">
            </div>
          </div>
        </SkyLight>
        {/*Skylight for Add attachments in edit page*/}
        <SkyLight dialogStyles={addattactments} hideOnOverlayClicked ref="addattachments">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">assignment</i>
              </div>
              <h4>Add Attachments</h4>

              <form className="padding-10" onSubmit={this.updateAttachments}>
                {/* Documents */}
                <div className="row margin-tb-15">

                  <div className="col-sm-12">
                    <label className="control-label label-color" >Documents :</label>
                  </div>

                </div>
                <div className="row">

                  <div className="col-sm-12 margin-padding-0">
                    <div className="input-group display-block">
                      <div className="form-group label-floating">
                        <ol className="ol-styling">
                          {this.displayList()}
                        </ol>
                        <label style={{ backgroundColor: '#E82062', color: 'white', padding: 10, borderRadius: 30, pointer: 'cursor', fontSize: 12 }}>
                          SELECT FILES
                          <FileUploader
                                hidden
                                accept=".gif, .jpg, .png, .pdf, .doc, .docx"
                                name="attachments"
                                multiple
                                storageRef={firebase.app.storage().ref('meet_docs')}
                                onUploadStart={this.handleUploadStart}
                                onUploadError={this.handleUploadError}
                                onUploadSuccess={this.handleUploadSuccess}
                                onProgress={this.handleProgress}
                            />
                        </label>
                        <Indicator show={this.state.isUploading} />
                      </div>
                    </div>
                  </div>

                </div>
                {/* Documents */}
                <div className="row">
                  <div className="col-sm-12 text-align-right">
                    <button className="btn btn-rose edit-attach-btn" value="Save"> <i className="material-icons">save</i> Save</button>
                    <button onClick={this.cancelAttachments} className="btn btn-rose edit-attach-btn" value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/* End Skylight for Add attachments in edit page*/}

      </div>
    )
  }

}
export default MeetList;

