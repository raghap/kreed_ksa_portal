import * as firebase from 'firebase';
require("firebase/firestore");

var collectionMeta={
	"aquaticsID/India/associations/KSA/clubs":{
		"fields":{
			"name":"Club Name",
			"address":"Club Address",
			"contact_name":"Contact Name",
			"email":"Contact Email",
			"mobile_number":"Contact Mobile Number",
			"payment_due":"Payment Due From Club",
			"payment_due_date":"Last Date For Payment"
		},
		"collections":[]
	},
	"athletes":{
		"fields":{
				"Name":"Athlete Name",
				"Email":"Contact Email",
				"Mobile Number":"Contact Mobile Number",
				"Aadhar":"UIDAI Number"
		},
		"collections":[]
}
}
module.exports = collectionMeta;
